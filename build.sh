rm -rf build
mkdir build
cd build
qmake ../Tool.pro
make

cd ..
rm -rf bin
mkdir bin
mkdir bin/assets
cp build/Tool bin
cp assets/OBJ/*.obj bin/assets