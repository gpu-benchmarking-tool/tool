rm -rf src/libs

git submodule update --init
git submodule update --remote

cd libs/opengl-engine
bash -i setup.sh
cd ..
cd vulkan-engine
bash -i setup.sh


cd ../../src
rm -rf libs
mkdir libs
cp ../libs/opengl-engine/src/* libs/ -r
cp ../libs/vulkan-engine/src/* libs/ -r
rm libs/main.cpp
cd ..
