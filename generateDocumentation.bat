git submodule update --init --recursive
git submodule update --remote --recursive
doxygen Doxygen
cd doc\latex
pdflatex refman -interaction=nonstopmode
cd ..\..
copy doc\latex\refman.pdf doc
cd doc
ren refman.pdf GPU-Benchmarking-Tool.pdf