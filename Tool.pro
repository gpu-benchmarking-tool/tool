#-------------------------------------------------
#
# Project created by QtCreator 2019-06-19T16:01:39
#
#-------------------------------------------------

QT       += core gui opengl charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Tool
TEMPLATE = app

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp
CONFIG += c++11


SOURCES += src/main.cpp\
    src/libs/getgpuinfo.cpp \
    src/libs/glwidget.cpp \
    src/libs/glwindow.cpp \
    src/libs/mesh.cpp \
    src/libs/renderer.cpp \
    src/libs/setvkinfo.cpp \
    src/libs/shader.cpp \
    src/libs/vulkanwindow.cpp \
    src/mainwindow.cpp \
    src/runners.cpp \
    src/updateGLresults.cpp \
    src/updateVKresults.cpp

HEADERS  += src/mainwindow.h \
    src/gldescriptions.hpp \
    src/libs/glwidget.h \
    src/libs/glwindow.h \
    src/libs/mesh.h \
    src/libs/model.hpp \
    src/libs/renderer.h \
    src/libs/shader.h \
    src/libs/vulkanwindow.h \
    src/vkdescriptions.hpp


FORMS    += \
    src/libs/glwindow.ui \
    src/toolwindow.ui \


RESOURCES += \
    libs/opengl-engine/glresources.qrc \
    libs/vulkan-engine/vulkanresources.qrc \
    mainwindow.qrc


## OPENGL ENGINE
#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/src/libs/ -lOpenGL-Engine
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/src/libs/ -lOpenGL-Engine
#else:unix: LIBS += -L$$PWD/src/libs/ -lOpenGL-Engine

#INCLUDEPATH += $$PWD/src/libs
#DEPENDPATH += $$PWD/src/libs

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libOpenGL-Engine.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libOpenGL-Engine.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/OpenGL-Engine.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/OpenGL-Engine.lib
#else:unix: PRE_TARGETDEPS += $$PWD/src/libs/libOpenGL-Engine.a


## VULKAN ENGINE
#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/src/libs/ -lVulkan-Engine
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/src/libs/ -lVulkan-Engine
#else:unix: LIBS += -L$$PWD/src/libs/ -lVulkan-Engine

#INCLUDEPATH += $$PWD/src/libs
#DEPENDPATH += $$PWD/src/libs

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libVulkan-Engine.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libVulkan-Engine.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/Vulkan-Engine.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/Vulkan-Engine.lib
#else:unix: PRE_TARGETDEPS += $$PWD/src/libs/libVulkan-Engine.a


# OBJ LOADER
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/src/libs/libs/ -lobj-loader
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/src/libs/libs/ -lobj-loader
else:unix: LIBS += -L$$PWD/src/libs/libs/ -lobj-loader

INCLUDEPATH += $$PWD/src/libs/libs
DEPENDPATH += $$PWD/src/libs/libs

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/libobj-loader.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/libobj-loader.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/obj-loader.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/obj-loader.lib
else:unix: PRE_TARGETDEPS += $$PWD/src/libs/libs/libobj-loader.a


# PROFILER
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/src/libs/libs/ -lprofiler-library
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/src/libs/libs/ -lprofiler-library
else:unix: LIBS += -L$$PWD/src/libs/libs/ -lprofiler-library

INCLUDEPATH += $$PWD/src/libs/libs
DEPENDPATH += $$PWD/src/libs/libs

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/libprofiler-library.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/libprofiler-library.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/profiler-library.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/src/libs/libs/profiler-library.lib
else:unix: PRE_TARGETDEPS += $$PWD/src/libs/libs/libprofiler-library.a

