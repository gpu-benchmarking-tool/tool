cd src
rmdir /s /Q libs
cd ..

git submodule update --init
git submodule update --remote

cd libs\opengl-engine
call setup.bat
cd ..\..
cd vulkan-engine
call setup.bat
cd ..

cd ..\..\src
rmdir /s /Q libs
mkdir libs
xcopy ..\libs\opengl-engine\src\* libs /E
xcopy ..\libs\vulkan-engine\src\* libs /E /y
cd ..