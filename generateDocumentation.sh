git submodule update --init --recursive
git submodule update --remote --recursive
doxygen Doxygen
cd doc/latex
pdflatex refman -interaction=nonstopmode
cd ../..
mv doc/latex/refman.pdf doc/GPU-Benchmarking-Tool.pdf