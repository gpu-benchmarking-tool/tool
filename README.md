# GPU Benchmarking Tool

*This repository is part of the [**GPU Benchmarking Tool**](https://gpu-benchmarking-documentation.firebaseapp.com/) project.*  

This component is the UI Tool used to run the benchmarking.

## Compile and run (Windows, Linux and macOS)
- Make sure you have *git* installed
    - [Git for Windows](https://git-scm.com/download/win)
    - [Git for Linux / macOS](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Make sure *cmake* is installed
    - [Cmake website](https://cmake.org/install/)
- Install [Qt 5.13](https://www.qt.io/download#)
- On Windows only, you need to install *MinGW* and add it to the path
    - [MinGW website](http://www.mingw.org/wiki/Install_MinGW)
- Make sure [windeployqt](https://doc.qt.io/qt-5/windows-deployment.html) is in your path
- Run `setup.bat` to setup the repository on Windows or `setup.sh` to setup on Linux / macOS
- Run `build.bat` on Windows or `build.sh` on Linux / macOS to compile the Engine
- The output of the compilation can be found in the `bin` folder


## Compile and run (DEC 10 Computers)
- Clone the repository with `git clone https://gitlab.com/gpu-benchmarking-tool/tool.git`
- Run `./dec10Install.sh`
- Go to the bin directory with `cd bin`
- Execute `./Tool`


## Structure of the repository
- `assets` contains the repository icon
- `doc` contains the documentation
- `models` contains the cube used for the tests
- `src` contains the source code
- `.gitlab-ci.yml` contains the Continuous Integration configuration
- `setup.bat` and `setup.sh` are used to setup the dependencies
- `build.bat` and `build.sh` are used to compile the project
- `Doxygen` contains the odumentation generation configuration
- `generateDocumentation.bat` and `generateDocumentation.sh` are used to generate the documentation
- `libs` contains the imported libraries



## Generate the documentation
- To generate the documentation, make sure you have [Doxygen](http://www.doxygen.nl/manual/install.html) installed as well as [LaTeX](https://www.latex-project.org/get/)
- Run `generateDocumentation.bat` on Windows or `generateDocumentation.sh` on Linux / macOS to generate the pdf documentation
- The documentation will be generated in the `doc` folder