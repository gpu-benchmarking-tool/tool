/*__________________________________________________
 |                                                  |
 |  File: mainwindow.cpp                            |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: MainWindow class.                  |
 |_________________________________________________*/



#include "mainwindow.h"
#include "ui_toolwindow.h"
#include <QSysInfo>


// Constructor.
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {

    // Setyp the UI
    ui->setupUi(this);

    // Connect the buttons
    connectButtons();

    // Set general info
    ui->cpuArch->setText(QSysInfo::currentCpuArchitecture());
    ui->kernelType->setText(QSysInfo::kernelType());
    ui->kernelVersion->setText(QSysInfo::kernelVersion());
    ui->machineHost->setText(QSysInfo::machineHostName());
    ui->osLabel->setText(QSysInfo::productType().toUpper() + " " + QSysInfo::productVersion());

    // Set the FPS target spinbox
    spinBox = new QSpinBox();
    spinBox->setPrefix("Target framerate: ");
    spinBox->setSuffix(" FPS");
    spinBox->setRange(1, 240);
    spinBox->setValue(60);
    ui->toolBar->addWidget(spinBox);

    // Reset the application
    reset();

    // Setup the charts
    setupCharts();
}


// Destructor.
MainWindow::~MainWindow() {

    // Delete the ui
    delete ui;

    // Delete the GL resources
    for (int i = 0; i < 3; ++i) {
        delete glProfilers[i];
        delete glCharts[i].axisX;
        delete glCharts[i].axisY;
        delete glCharts[i].series;
        delete glCharts[i].chart;
        delete glCharts[i].chartView;
    }

    // Delete the Vulkan resources
    for (int i = 0; i < 2; ++i) {
        delete vkProfilers[i];
        delete vkCharts[i].axisX;
        delete vkCharts[i].axisY;
        delete vkCharts[i].series;
        delete vkCharts[i].chart;
        delete vkCharts[i].chartView;
    }

    // Delete the spin box
    delete spinBox;
}


// Resets the program.
void MainWindow::reset() {

    // Instantiate the profiler
    glProfilers[0] = new gpu::Profiler();
    glProfilers[1] = new gpu::Profiler();
    glProfilers[2] = new gpu::Profiler();
    vkProfilers[0] = new gpu::Profiler();
    vkProfilers[1] = new gpu::Profiler();

    // Set the stack widgets indexes
    ui->glResultsStack->setCurrentIndex(0);
    ui->vkResultsStack->setCurrentIndex(0);
    ui->infoStacked->setCurrentIndex(0);

    // Disable tabs
    ui->tabWidget->setTabEnabled(0, true);
    ui->tabWidget->setTabEnabled(1, false);
    ui->tabWidget->setTabEnabled(2, false);
    ui->tabWidget->setTabEnabled(3, false);
    ui->tabWidget->setTabEnabled(4, false);
    ui->tabWidget->setTabEnabled(5, false);
    ui->tabWidget->setTabEnabled(6, false);

    // Set the bools
    hasGLResults = false;
    hasVulkanResults = false;
    twoRuns = false;

    // Reset the progress bar
    ui->progressBar->setValue(0);

    // Empty the profilers
    glProfilers[0]->clear();
    glProfilers[1]->clear();
    glProfilers[2]->clear();
    vkProfilers[0]->clear();
    vkProfilers[1]->clear();

    // Empty the info tables
    ui->glInfoTable->setRowCount(0);
    ui->vkInfoTable->setRowCount(0);
}


// Connect the buttons.
void MainWindow::connectButtons() {

    // Connect the actions
    connect(ui->actionRun, &QAction::triggered, [&]() { reset(); run(); });
    connect(ui->actionRun_OpenGL, &QAction::triggered, [&]() { reset(); runGL(); });
    connect(ui->actionRun_Vulkan, &QAction::triggered, [&]() { reset(); runVk(); });
    connect(ui->actionExport_results, SIGNAL(triggered()), this, SLOT(exportResults()));
    connect(ui->actionClose, &QAction::triggered, [&]() { close(); });
    connect(ui->action_doc, &QAction::triggered, [&]() { QDesktopServices::openUrl(QUrl("https://tiny.cc/GPU-Benchmarking")); });

    // Connect the navigation buttons
    connect(ui->p0n, &QPushButton::clicked, [&]() { ui->glResultsStack->setCurrentIndex(1); });
    connect(ui->p1p, &QPushButton::clicked, [&]() { ui->glResultsStack->setCurrentIndex(0); });
    connect(ui->p1n, &QPushButton::clicked, [&]() { ui->glResultsStack->setCurrentIndex(2); });
    connect(ui->p2p, &QPushButton::clicked, [&]() { ui->glResultsStack->setCurrentIndex(1); });
    connect(ui->p2n, &QPushButton::clicked, [&]() { ui->glResultsStack->setCurrentIndex(3); });
    connect(ui->p3p, &QPushButton::clicked, [&]() { ui->glResultsStack->setCurrentIndex(2); });
    connect(ui->p0n_1, &QPushButton::clicked, [&]() { ui->vkResultsStack->setCurrentIndex(1); });
    connect(ui->p1p_1, &QPushButton::clicked, [&]() { ui->vkResultsStack->setCurrentIndex(0); });
    connect(ui->p1n_1, &QPushButton::clicked, [&]() { ui->vkResultsStack->setCurrentIndex(2); });
    connect(ui->p2p_1, &QPushButton::clicked, [&]() { ui->vkResultsStack->setCurrentIndex(1); });
}


// Setups the charts.
void MainWindow::setupCharts() {

    // Create the charts
    glCharts[0].chart = new QChart();
    glCharts[1].chart = new QChart();
    glCharts[2].chart = new QChart();
    vkCharts[0].chart = new QChart();
    vkCharts[1].chart = new QChart();

    // Create the series
    glCharts[0].series = new QLineSeries(glCharts[0].chart);
    glCharts[1].series = new QLineSeries(glCharts[1].chart);
    glCharts[2].series = new QLineSeries(glCharts[2].chart);
    vkCharts[0].series = new QLineSeries(vkCharts[0].chart);
    vkCharts[1].series = new QLineSeries(vkCharts[1].chart);

    // Add the series to the charts
    glCharts[0].chart->addSeries(glCharts[0].series);
    glCharts[1].chart->addSeries(glCharts[1].series);
    glCharts[2].chart->addSeries(glCharts[2].series);
    vkCharts[0].chart->addSeries(vkCharts[0].series);
    vkCharts[1].chart->addSeries(vkCharts[1].series);

    // Create the X axes
    glCharts[0].axisX = new QValueAxis();
    glCharts[1].axisX = new QValueAxis();
    glCharts[2].axisX = new QValueAxis();
    vkCharts[0].axisX = new QValueAxis();
    vkCharts[1].axisX = new QValueAxis();

    // Create the Y axes
    glCharts[0].axisY = new QValueAxis();
    glCharts[1].axisY = new QValueAxis();
    glCharts[2].axisY = new QValueAxis();
    vkCharts[0].axisY = new QValueAxis();
    vkCharts[1].axisY = new QValueAxis();

    // Set the ticks X axes
    glCharts[0].axisX->setTickCount(10);
    glCharts[1].axisX->setTickCount(10);
    glCharts[2].axisX->setTickCount(10);
    vkCharts[0].axisX->setTickCount(10);
    vkCharts[1].axisX->setTickCount(10);

    // Set the ticks and labels Y axes
    glCharts[0].axisY->setTickCount(10);
    glCharts[1].axisY->setTickCount(10);
    glCharts[2].axisY->setTickCount(10);
    vkCharts[0].axisY->setTickCount(10);
    vkCharts[1].axisY->setTickCount(10);
    glCharts[0].axisY->setLabelFormat("%.0f");
    glCharts[1].axisY->setLabelFormat("%.0f");
    glCharts[2].axisY->setLabelFormat("%.0f");
    vkCharts[0].axisY->setLabelFormat("%.0f");
    vkCharts[1].axisY->setLabelFormat("%.0f");

    // Set the X axes titles
    glCharts[0].axisX->setTitleText("Number of triangles rendered");
    glCharts[1].axisX->setTitleText("Number of triangles rendered");
    glCharts[2].axisX->setTitleText("Number of triangles rendered");
    vkCharts[0].axisX->setTitleText("Number of triangles rendered");
    vkCharts[1].axisX->setTitleText("Number of triangles rendered");

    // Set the Y axes titles
    glCharts[0].axisY->setTitleText("Time in milliseconds");
    glCharts[1].axisY->setTitleText("Time in milliseconds");
    glCharts[2].axisY->setTitleText("Time in milliseconds");
    vkCharts[0].axisY->setTitleText("Time in milliseconds");
    vkCharts[1].axisY->setTitleText("Time in milliseconds");

    // Add the X axes
    glCharts[0].chart->addAxis(glCharts[0].axisX, Qt::AlignBottom);
    glCharts[1].chart->addAxis(glCharts[1].axisX, Qt::AlignBottom);
    glCharts[2].chart->addAxis(glCharts[2].axisX, Qt::AlignBottom);
    vkCharts[0].chart->addAxis(vkCharts[0].axisX, Qt::AlignBottom);
    vkCharts[1].chart->addAxis(vkCharts[1].axisX, Qt::AlignBottom);
    glCharts[0].series->attachAxis(glCharts[0].axisX);
    glCharts[1].series->attachAxis(glCharts[1].axisX);
    glCharts[2].series->attachAxis(glCharts[2].axisX);
    vkCharts[0].series->attachAxis(vkCharts[0].axisX);
    vkCharts[1].series->attachAxis(vkCharts[1].axisX);

    // Add the Y axes
    glCharts[0].chart->addAxis(glCharts[0].axisY, Qt::AlignLeft);
    glCharts[1].chart->addAxis(glCharts[1].axisY, Qt::AlignLeft);
    glCharts[2].chart->addAxis(glCharts[2].axisY, Qt::AlignLeft);
    vkCharts[0].chart->addAxis(vkCharts[0].axisY, Qt::AlignLeft);
    vkCharts[1].chart->addAxis(vkCharts[1].axisY, Qt::AlignLeft);
    glCharts[0].series->attachAxis(glCharts[0].axisY);
    glCharts[1].series->attachAxis(glCharts[1].axisY);
    glCharts[2].series->attachAxis(glCharts[2].axisY);
    vkCharts[0].series->attachAxis(vkCharts[0].axisY);
    vkCharts[1].series->attachAxis(vkCharts[1].axisY);

    // Instantiate the chart views
    glCharts[0].chartView = new QChartView(glCharts[0].chart);
    glCharts[1].chartView = new QChartView(glCharts[1].chart);
    glCharts[2].chartView = new QChartView(glCharts[2].chart);
    vkCharts[0].chartView = new QChartView(vkCharts[0].chart);
    vkCharts[1].chartView = new QChartView(vkCharts[1].chart);

    // Add the results to the layout
    ui->glResult1->addWidget(glCharts[0].chartView);
    ui->glResult2->addWidget(glCharts[1].chartView);
    ui->glResult3->addWidget(glCharts[2].chartView);
    ui->vkResult1->addWidget(vkCharts[0].chartView);
    ui->vkResult2->addWidget(vkCharts[1].chartView);

    // Remove the legends
    glCharts[0].chart->legend()->markers(glCharts[0].series)[0]->setVisible(false);
    glCharts[1].chart->legend()->markers(glCharts[1].series)[0]->setVisible(false);
    glCharts[2].chart->legend()->markers(glCharts[2].series)[0]->setVisible(false);
    vkCharts[0].chart->legend()->markers(vkCharts[0].series)[0]->setVisible(false);
    vkCharts[1].chart->legend()->markers(vkCharts[1].series)[0]->setVisible(false);
}


// Exports the results as pngs.
void MainWindow::exportResults() {

    // Choose the directory
    QString directory = QFileDialog::getExistingDirectory(this, tr("Select directory"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (directory == "") return;


    // Set folder name
    QDateTime date = QDateTime::currentDateTime();
    directory += "/Benchmarking_" + date.toString("dd-MM-yyyy_HH-mm") + " - " + QString::number(spinBox->value()) + "FPS";
    QDir().mkdir(directory);

    // Save GL results
    if (hasGLResults) {
        ui->glGrabArea->grab().save(directory + "/OpenGL_result.png");
        glCharts[0].chartView->grab().save(directory + "/OpenGL_graph_1.png");
        glCharts[1].chartView->grab().save(directory + "/OpenGL_graph_2.png");
        glCharts[2].chartView->grab().save(directory + "/OpenGL_graph_3.png");
        glProfilers[0]->save((directory + "/OpenGL_data_1.csv").toStdString());
        glProfilers[1]->save((directory + "/OpenGL_data_2.csv").toStdString());
        glProfilers[2]->save((directory + "/OpenGL_data_3.csv").toStdString());
    }

    // Save Vulkan results
    if (hasVulkanResults) {
        ui->vkGrabArea->grab().save(directory + "/Vulkan_result.png");
        vkCharts[0].chartView->grab().save(directory + "/Vulkan_graph_1.png");
        vkCharts[1].chartView->grab().save(directory + "/Vulkan_graph_2.png");
        vkProfilers[0]->save((directory + "/Vulkan_data_1.csv").toStdString());
        vkProfilers[1]->save((directory + "/Vulkan_data_2.csv").toStdString());
    }
}
