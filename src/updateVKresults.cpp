/*__________________________________________________
 |                                                  |
 |    File: updateVKresults.cpp                     |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Updates the Vulkan results at    |
 |    the end of the benchmarking.                  |
 |_________________________________________________*/



#include "mainwindow.h"
#include "ui_toolwindow.h"
#include "vkdescriptions.hpp"


// Macro used to check if we have a result or not
#define DISPLAY(r) static_cast<int>(r) != -1 ? QString::number(r) : "No data"


// Updates the Vulkan charts.
void MainWindow::updateVkCharts() {

    // Get the profiler data
    std::vector<std::vector<unsigned>> data[2];
    data[0] = vkProfilers[0]->getData();
    data[1] = vkProfilers[1]->getData();

    // Get the max value
    unsigned highest;

    if (data[0].size() >= data[1].size()) highest = data[0][data[0].size() - 1][1];
    else highest = data[1][data[1].size() - 1][1];

    // Order of magnitude
    unsigned mult = 10;
    short mag = 1;

    while (highest / mult >= 100) {
        mult *= 10;
        ++mag;
    }

    mult /= 10;
    --mag;


    // Clear the series
    vkCharts[0].series->clear();
    vkCharts[1].series->clear();

    // Add the data to the series
    for (unsigned i = 0; i < data[0].size(); ++i)
        vkCharts[0].series->append(QPointF(data[0][i][1] / static_cast<qreal>(mult), data[0][i][2]));

    for (unsigned i = 0; i < data[1].size(); ++i)
        vkCharts[1].series->append(QPointF(data[1][i][1] / static_cast<qreal>(mult), data[1][i][2]));

    // Set the X ranges
    vkCharts[0].axisX->setRange(0, data[0][data[0].size() - 1][1] / static_cast<qreal>(mult));
    vkCharts[1].axisX->setRange(0, data[1][data[1].size() - 1][1] / static_cast<qreal>(mult));

    // Set the Y ranges
    vkCharts[0].axisY->setRange(0, data[0][data[0].size() - 1][2]);
    vkCharts[1].axisY->setRange(0, data[1][data[1].size() - 1][2]);

    // Set the labels X axes
    QString format = "%.0f E+" + QString::number(mag);
    vkCharts[0].axisX->setLabelFormat(format.toUtf8());
    vkCharts[1].axisX->setLabelFormat(format.toUtf8());
}


// Updates the Vulkan result card.
void MainWindow::updateVkCard() {

    // Compute the score
    unsigned v0 = vkProfilers[0]->getData()[vkProfilers[0]->getData().size() - 2][1];
    unsigned v1 = vkProfilers[1]->getData()[vkProfilers[1]->getData().size() - 2][1];

    unsigned m = (v0 + v1) / 2;

    unsigned score = (m / 20000) * (spinBox->value() / 10.);
    ui->scoreVk->setText(QString::number(score));

    // Set the extensions tab
    QString extensions = "";
    for (unsigned i = 0; i < vkProfilers[0]->getVkExtensions().size(); ++i) extensions += QString::fromStdString(vkProfilers[0]->getVkExtensions()[i]) + "\n";
    ui->vkExtensions->setText(extensions);

    // Set the labels
    ui->osNameVk->setText(QSysInfo::productType().toUpper() + " " + QSysInfo::productVersion());
    ui->cpuLabelVk->setText(QSysInfo::currentCpuArchitecture());
    ui->vkDeviceName->setText(QString::fromStdString(vkProfilers[0]->getVkDeviceName()));
    ui->vkVendorID->setText(QString::number(vkProfilers[0]->getVkVendorID()));
    ui->vkAPIVersion->setText(QString::number(vkProfilers[0]->getVkApiVersion()));
    ui->vkDeviceID->setText(QString::number(vkProfilers[0]->getVkDeviceID()));
    ui->vkFPS->setText(QString::number(spinBox->value()));

    // Set the text summary
    ui->vkTextSummary->setHtml(
        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">"
        "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">"
        "p, li { white-space: pre-wrap; }"
        "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">"
        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"
        "<span style=\" font-size:11pt; color:#000000;\">It can render up to </span><span style=\" font-size:13pt; font-weight:600; color:#ff1261;\">" +
        QString::number(m) + "</span><span style=\" font-size:11pt; color:#000000;\"> triangles per frame in order to achieve a target refresh rate of " + QString::number(spinBox->value()) + " frames per second, "
        "i. e. rendering a frame in less than " + QString::number(1000./spinBox->value()) + "ms. Considering an average of 20 000 triangles per character, "
        "</span><span style=\" font-size:13pt; font-weight:600; color:#ff1261;\">" +
        QString::number(m / 20000) + "</span><span style=\" font-size:11pt; color:#000000;\"> characters can be rendered at the same time without being simplified."
        "</span></p></body></html>");
}


// Updates the vulkan info table.
void MainWindow::updateVkInfoTable() {

    int i = 0;
    VkDescriptions desc;


    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Image Dimension1D"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxImageDimension1D)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxImageDimension1D.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxImageDimension1D.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Image Dimension2D"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxImageDimension2D)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxImageDimension2D.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxImageDimension2D.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Image Dimension3D"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxImageDimension3D)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxImageDimension3D.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxImageDimension3D.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Image Dimension Cube"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxImageDimensionCube)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxImageDimensionCube.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxImageDimensionCube.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Image Array Layers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxImageArrayLayers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxImageArrayLayers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxImageArrayLayers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Texel Buffer Elements"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTexelBufferElements)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTexelBufferElements.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTexelBufferElements.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Uniform Buffer Range"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxUniformBufferRange)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxUniformBufferRange.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxUniformBufferRange.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Storage Buffer Range"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxStorageBufferRange)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxStorageBufferRange.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxStorageBufferRange.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Push Constants Size"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPushConstantsSize)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPushConstantsSize.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPushConstantsSize.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Memory Allocation Count"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxMemoryAllocationCount)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxMemoryAllocationCount.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxMemoryAllocationCount.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Sampler Allocation Count"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxSamplerAllocationCount)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxSamplerAllocationCount.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxSamplerAllocationCount.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Buffer Image Granularity"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.bufferImageGranularity)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.bufferImageGranularity.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.bufferImageGranularity.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Sparse Address Space Size"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.sparseAddressSpaceSize)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.sparseAddressSpaceSize.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.sparseAddressSpaceSize.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Bound Descriptor Sets"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxBoundDescriptorSets)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxBoundDescriptorSets.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxBoundDescriptorSets.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Per Stage Descriptor Samplers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPerStageDescriptorSamplers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPerStageDescriptorSamplers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPerStageDescriptorSamplers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Per Stage Descriptor Uniform Buffers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPerStageDescriptorUniformBuffers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPerStageDescriptorUniformBuffers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPerStageDescriptorUniformBuffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Per Stage Descriptor Storage Buffers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPerStageDescriptorStorageBuffers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPerStageDescriptorStorageBuffers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPerStageDescriptorStorageBuffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Per Stage Descriptor Sampled Images"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPerStageDescriptorSampledImages)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPerStageDescriptorSampledImages.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPerStageDescriptorSampledImages.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Per Stage Descriptor Storage Images"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPerStageDescriptorStorageImages)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPerStageDescriptorStorageImages.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPerStageDescriptorStorageImages.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Per Stage Descriptor Input Attachments"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPerStageDescriptorInputAttachments)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPerStageDescriptorInputAttachments.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPerStageDescriptorInputAttachments.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Per Stage Resources"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxPerStageResources)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxPerStageResources.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxPerStageResources.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Samplers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetSamplers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetSamplers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetSamplers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Uniform Buffers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetUniformBuffers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetUniformBuffers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetUniformBuffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Uniform Buffers Dynamic"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetUniformBuffersDynamic)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetUniformBuffersDynamic.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetUniformBuffersDynamic.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Storage Buffers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetStorageBuffers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetStorageBuffers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetStorageBuffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Storage Buffers Dynamic"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetStorageBuffersDynamic)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetStorageBuffersDynamic.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetStorageBuffersDynamic.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Sampled Images"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetSampledImages)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetSampledImages.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetSampledImages.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Storage Images"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetStorageImages)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetStorageImages.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetStorageImages.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Descriptor Set Input Attachments"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDescriptorSetInputAttachments)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDescriptorSetInputAttachments.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDescriptorSetInputAttachments.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Vertex Input Attributes"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxVertexInputAttributes)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxVertexInputAttributes.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxVertexInputAttributes.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Vertex Input Bindings"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxVertexInputBindings)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxVertexInputBindings.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxVertexInputBindings.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Vertex Input Attribute Offset"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxVertexInputAttributeOffset)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxVertexInputAttributeOffset.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxVertexInputAttributeOffset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Vertex Input Binding Stride"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxVertexInputBindingStride)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxVertexInputBindingStride.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxVertexInputBindingStride.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Vertex Output Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxVertexOutputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxVertexOutputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxVertexOutputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Generation Level"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationGenerationLevel)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationGenerationLevel.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationGenerationLevel.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Patch Size"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationPatchSize)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationPatchSize.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationPatchSize.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Control Per Vertex Input Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationControlPerVertexInputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationControlPerVertexInputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationControlPerVertexInputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Control Per Vertex Output Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationControlPerVertexOutputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationControlPerVertexOutputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationControlPerVertexOutputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Control Per Patch Output Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationControlPerPatchOutputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationControlPerPatchOutputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationControlPerPatchOutputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Control Total Output Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationControlTotalOutputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationControlTotalOutputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationControlTotalOutputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Evaluation Input Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationEvaluationInputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationEvaluationInputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationEvaluationInputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Tessellation Evaluation Output Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTessellationEvaluationOutputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTessellationEvaluationOutputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTessellationEvaluationOutputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Geometry Shader Invocations"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxGeometryShaderInvocations)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxGeometryShaderInvocations.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxGeometryShaderInvocations.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Geometry Input Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxGeometryInputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxGeometryInputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxGeometryInputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Geometry Output Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxGeometryOutputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxGeometryOutputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxGeometryOutputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Geometry Output Vertices"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxGeometryOutputVertices)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxGeometryOutputVertices.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxGeometryOutputVertices.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Geometry Total Output Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxGeometryTotalOutputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxGeometryTotalOutputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxGeometryTotalOutputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Fragment Input Components"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxFragmentInputComponents)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxFragmentInputComponents.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxFragmentInputComponents.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Fragment Output Attachments"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxFragmentOutputAttachments)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxFragmentOutputAttachments.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxFragmentOutputAttachments.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Fragment Dual Src Attachments"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxFragmentDualSrcAttachments)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxFragmentDualSrcAttachments.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxFragmentDualSrcAttachments.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Fragment Combined Output Resources"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxFragmentCombinedOutputResources)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxFragmentCombinedOutputResources.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxFragmentCombinedOutputResources.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Shared Memory Size"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeSharedMemorySize)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeSharedMemorySize.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeSharedMemorySize.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Work Group Count X"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeWorkGroupCountX)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeWorkGroupCountX.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeWorkGroupCountX.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Work Group Count Y"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeWorkGroupCountY)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeWorkGroupCountY.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeWorkGroupCountY.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Work Group Count Z"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeWorkGroupCountZ)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeWorkGroupCountZ.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeWorkGroupCountZ.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Work Group Invocations"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeWorkGroupInvocations)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeWorkGroupInvocations.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeWorkGroupInvocations.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Work Group Size X"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeWorkGroupSizeX)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeWorkGroupSizeX.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeWorkGroupSizeX.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Work Group Size Y"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeWorkGroupSizeY)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeWorkGroupSizeY.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeWorkGroupSizeY.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Compute Work Group Size Z"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxComputeWorkGroupSizeZ)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxComputeWorkGroupSizeZ.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxComputeWorkGroupSizeZ.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Sub Pixel Precision Bits"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.subPixelPrecisionBits)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.subPixelPrecisionBits.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.subPixelPrecisionBits.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Sub Texel Precision Bits"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.subTexelPrecisionBits)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.subTexelPrecisionBits.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.subTexelPrecisionBits.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Mipmap Precision Bits"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.mipmapPrecisionBits)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.mipmapPrecisionBits.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.mipmapPrecisionBits.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Draw Indexed Index Value"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDrawIndexedIndexValue)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDrawIndexedIndexValue.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDrawIndexedIndexValue.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Draw Indirect Count"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxDrawIndirectCount)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxDrawIndirectCount.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxDrawIndirectCount.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Sampler Lod Bias"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.maxSamplerLodBias))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxSamplerLodBias.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxSamplerLodBias.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Sampler Anisotropy"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.maxSamplerAnisotropy))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxSamplerAnisotropy.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxSamplerAnisotropy.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Viewports"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxViewports)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxViewports.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxViewports.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Viewport Dimensions X"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxViewportDimensionsX)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxViewportDimensionsX.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxViewportDimensionsX.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Viewport Dimensions Y"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxViewportDimensionsY)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxViewportDimensionsY.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxViewportDimensionsY.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Viewport Bounds Range X"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.viewportBoundsRangeX))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.viewportBoundsRangeX.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.viewportBoundsRangeX.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Viewport Bounds Range Y"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.viewportBoundsRangeY))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.viewportBoundsRangeY.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.viewportBoundsRangeY.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Viewport Sub Pixel Bits"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.viewportSubPixelBits)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.viewportSubPixelBits.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.viewportSubPixelBits.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Min Memory Map Alignment"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.minMemoryMapAlignment)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.minMemoryMapAlignment.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.minMemoryMapAlignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Min Texel Buffer Offset Alignment"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.minTexelBufferOffsetAlignment)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.minTexelBufferOffsetAlignment.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.minTexelBufferOffsetAlignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Min Uniform Buffer Offset Alignment"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.minUniformBufferOffsetAlignment)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.minUniformBufferOffsetAlignment.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.minUniformBufferOffsetAlignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Min Storage Buffer Offset Alignment"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.minStorageBufferOffsetAlignment)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.minStorageBufferOffsetAlignment.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.minStorageBufferOffsetAlignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Min Texel Offset"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.minTexelOffset)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.minTexelOffset.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.minTexelOffset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Texel Offset"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTexelOffset)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTexelOffset.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTexelOffset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Min Texel Gather Offset"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.minTexelGatherOffset)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.minTexelGatherOffset.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.minTexelGatherOffset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Texel Gather Offset"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxTexelGatherOffset)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxTexelGatherOffset.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxTexelGatherOffset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Min Interpolation Offset"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.minInterpolationOffset))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.minInterpolationOffset.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.minInterpolationOffset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Interpolation Offset"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.maxInterpolationOffset))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxInterpolationOffset.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxInterpolationOffset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Sub Pixel Interpolation Offset Bits"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.subPixelInterpolationOffsetBits)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.subPixelInterpolationOffsetBits.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.subPixelInterpolationOffsetBits.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Framebuffer Width"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxFramebufferWidth)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxFramebufferWidth.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxFramebufferWidth.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Framebuffer Height"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxFramebufferHeight)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxFramebufferHeight.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxFramebufferHeight.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Framebuffer Layers"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxFramebufferLayers)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxFramebufferLayers.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxFramebufferLayers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Color Attachments"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxColorAttachments)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxColorAttachments.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxColorAttachments.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Sample Mask Words"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxSampleMaskWords)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxSampleMaskWords.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxSampleMaskWords.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Timestamp Period"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.timestampPeriod))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.timestampPeriod.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.timestampPeriod.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Clip Distances"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxClipDistances)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxClipDistances.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxClipDistances.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Cull Distances"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxCullDistances)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxCullDistances.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxCullDistances.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Max Combined Clip And Cull Distances"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.maxCombinedClipAndCullDistances)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.maxCombinedClipAndCullDistances.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.maxCombinedClipAndCullDistances.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Discrete Queue Priorities"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.discreteQueuePriorities)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.discreteQueuePriorities.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.discreteQueuePriorities.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Point Size Range X"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.pointSizeRangeX))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.pointSizeRangeX.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.pointSizeRangeX.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Point Size Range Y"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.pointSizeRangeY))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.pointSizeRangeY.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.pointSizeRangeY.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Line Width Range X"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.lineWidthRangeX))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.lineWidthRangeX.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.lineWidthRangeX.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Line Width Range Y"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.lineWidthRangeY))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.lineWidthRangeY.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.lineWidthRangeY.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Point Size Granularity"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.pointSizeGranularity))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.pointSizeGranularity.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.pointSizeGranularity.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Line Width Granularity"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(static_cast<double>(vkProfilers[0]->vkInfo.lineWidthGranularity))));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.lineWidthGranularity.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.lineWidthGranularity.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Optimal Buffer Copy Offset Alignment"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.optimalBufferCopyOffsetAlignment)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.optimalBufferCopyOffsetAlignment.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.optimalBufferCopyOffsetAlignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Optimal Buffer Copy Row Pitch Alignment"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.optimalBufferCopyRowPitchAlignment)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.optimalBufferCopyRowPitchAlignment.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.optimalBufferCopyRowPitchAlignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->insertRow(i);
    ui->vkInfoTable->setItem(i, 0, new QTableWidgetItem("Non Coherent Atom Size"));
    ui->vkInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(vkProfilers[0]->vkInfo.nonCoherentAtomSize)));
    ui->vkInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.nonCoherentAtomSize.replace(". ", "\n")));
    ui->vkInfoTable->setRowHeight(i, (desc.nonCoherentAtomSize.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->vkInfoTable->horizontalHeader()->show();
}
