/*__________________________________________________
 |                                                  |
 |  File: main.cpp                                  |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Main function of the application.  |
 |_________________________________________________*/



#include <QApplication>
#include "mainwindow.h"


// Main function.
int main(int argc, char *argv[]) {

    // Initialise the application
    QApplication a(argc, argv);

    // Create the window
    MainWindow window;
    window.show();

    return a.exec();
}
