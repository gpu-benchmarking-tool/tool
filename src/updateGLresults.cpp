/*__________________________________________________
 |                                                  |
 |    File: updateGLresults.cpp                     |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Updates the OpenGL results at    |
 |    the end of the benchmarking.                  |
 |_________________________________________________*/



#include "mainwindow.h"
#include "ui_toolwindow.h"
#include "gldescriptions.hpp"


// Macro used to check if we have a result or not
#define DISPLAY(r) r != -1 ? QString::number(r) : "No data"


// Updates the OpenGL charts.
void MainWindow::updateGLCharts() {

    // Get the profiler data
    std::vector<std::vector<unsigned>> data[3];
    data[0] = glProfilers[0]->getData();
    data[1] = glProfilers[1]->getData();
    data[2] = glProfilers[2]->getData();

    // Get the max value
    unsigned highest;

    if (data[0].size() >= data[1].size()) {
        if (data[0].size() >= data[2].size()) highest = data[0][data[0].size() - 1][1];
        else highest = data[2][data[2].size() - 1][1];

    }
    else {
        if (data[1].size() >= data[2].size()) highest = data[1][data[1].size() - 1][1];
        else highest = data[2][data[2].size() - 1][1];
    }

    // Order of magnitude
    unsigned mult = 10;
    short mag = 1;

    while (highest / mult >= 100) {
        mult *= 10;
        ++mag;
    }

    mult /= 10;
    --mag;


    // Clear the series
    glCharts[0].series->clear();
    glCharts[1].series->clear();
    glCharts[2].series->clear();

    // Add the data to the series
    for (unsigned i = 0; i < data[0].size(); ++i)
        glCharts[0].series->append(QPointF(data[0][i][1] / static_cast<qreal>(mult), data[0][i][2]));

    for (unsigned i = 0; i < data[1].size(); ++i)
        glCharts[1].series->append(QPointF(data[1][i][1] / static_cast<qreal>(mult), data[1][i][2]));

    for (unsigned i = 0; i < data[2].size(); ++i)
        glCharts[2].series->append(QPointF(data[2][i][1] / static_cast<qreal>(mult), data[2][i][2]));

    // Set the X ranges
    glCharts[0].axisX->setRange(0, data[0][data[0].size() - 1][1] / static_cast<qreal>(mult));
    glCharts[1].axisX->setRange(0, data[1][data[1].size() - 1][1] / static_cast<qreal>(mult));
    glCharts[2].axisX->setRange(0, data[2][data[2].size() - 1][1] / static_cast<qreal>(mult));

    // Set the Y ranges
    glCharts[0].axisY->setRange(0, data[0][data[0].size() - 1][2]);
    glCharts[1].axisY->setRange(0, data[1][data[1].size() - 1][2]);
    glCharts[2].axisY->setRange(0, data[2][data[2].size() - 1][2]);

    // Set the ticks and labels X axes
    QString format = "%.0f E+" + QString::number(mag);
    glCharts[0].axisX->setLabelFormat(format.toUtf8());
    glCharts[1].axisX->setLabelFormat(format.toUtf8());
    glCharts[2].axisX->setLabelFormat(format.toUtf8());
}


// Updates the OpenGL result card.
void MainWindow::updateGLCard() {

    // Compute the score
    unsigned v0 = glProfilers[0]->getData()[glProfilers[0]->getData().size() - 2][1];
    unsigned v1 = glProfilers[1]->getData()[glProfilers[1]->getData().size() - 2][1];
    unsigned v2 = glProfilers[2]->getData()[glProfilers[2]->getData().size() - 2][1];

    unsigned m = (v0 + v1 + v2) / 3;

    unsigned score = (m / 20000) * (spinBox->value() / 10.);
    ui->scoreGL->setText(QString::number(score));

    // Set the extensions tab
    ui->glExtensions->setText(QString::fromStdString(glProfilers[0]->getGLExtensions()).replace(",", "\n").replace(" ", "\n"));

    // Set the labels
    ui->osName->setText(QSysInfo::productType().toUpper() + " " + QSysInfo::productVersion());
    ui->cpuLabel->setText(QSysInfo::currentCpuArchitecture());
    ui->vendor->setText(QString::fromStdString(glProfilers[0]->getVendor()));
    ui->renderer->setText(QString::fromStdString(glProfilers[0]->getRenderer()));
    ui->glVersion->setText(QString::fromStdString(glProfilers[0]->getGLVersion()).mid(0, ' '));
    ui->glShadingVersion->setText(QString::fromStdString(glProfilers[0]->getGLShadingLanguageVersion()));
    ui->FPSGL->setText(QString::number(spinBox->value()));

    // Set the text summary
    ui->glTextSummary->setHtml(
        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">"
        "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">"
        "p, li { white-space: pre-wrap; }"
        "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">"
        "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"
        "<span style=\" font-size:11pt; color:#000000;\">It can render up to </span><span style=\" font-size:13pt; font-weight:600; color:#ff1261;\">" +
        QString::number(m) + "</span><span style=\" font-size:11pt; color:#000000;\"> triangles per frame in order to achieve a target refresh rate of " + QString::number(spinBox->value()) + " frames per second, "
        "i. e. rendering a frame in less than " + QString::number(1000./spinBox->value()) + "ms. Considering an average of 20 000 triangles per character, "
        "</span><span style=\" font-size:13pt; font-weight:600; color:#ff1261;\">" +
        QString::number(m / 20000) + "</span><span style=\" font-size:11pt; color:#000000;\"> characters can be rendered at the same time without being simplified."
        "</span></p></body></html>");
}


// Updates the OpenGL info table.
void MainWindow::updateGLInfoTable() {

    int i = 0;
    GLDescriptions desc;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_ACTIVE_TEXTURE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_active_texture)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_active_texture.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_active_texture.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_ALIASED_LINE_WIDTH_RANGE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_aliased_line_width_range)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_aliased_line_width_range.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_aliased_line_width_range.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_ARRAY_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_array_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_array_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_array_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND_COLOR"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend_color)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend_color.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend_color.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND_DST_ALPHA"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend_dst_alpha)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend_dst_alpha.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend_dst_alpha.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND_DST_RGB"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend_dst_rgb)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend_dst_rgb.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend_dst_rgb.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND_EQUATION_RGB"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend_equation_rgb)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend_equation_rgb.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend_equation_rgb.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND_EQUATION_ALPHA"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend_equation_alpha)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend_equation_alpha.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend_equation_alpha.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND_SRC_ALPHA"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend_src_alpha)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend_src_alpha.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend_src_alpha.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_BLEND_SRC_RGB"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_blend_src_rgb)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_blend_src_rgb.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_blend_src_rgb.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_COLOR_CLEAR_VALUE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_color_clear_value)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_color_clear_value.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_color_clear_value.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_COLOR_LOGIC_OP"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_color_logic_op)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_color_logic_op.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_color_logic_op.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_COLOR_WRITEMASK"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_color_writemask)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_color_writemask.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_color_writemask.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_COMPRESSED_TEXTURE_FORMATS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_compressed_texture_formats)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_compressed_texture_formats.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_compressed_texture_formats.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_shader_storage_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_shader_storage_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_shader_storage_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_shader_storage_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_shader_storage_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_shader_storage_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_UNIFORM_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_uniform_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_uniform_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_uniform_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_texture_image_units)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_texture_image_units.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_texture_image_units.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_ATOMIC_COUNTERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_atomic_counters)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_atomic_counters.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_atomic_counters.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_atomic_counter_buffers)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_atomic_counter_buffers.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_atomic_counter_buffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_compute_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_compute_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_compute_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_WORK_GROUP_COUNT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_work_group_count)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_work_group_count.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_work_group_count.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMPUTE_WORK_GROUP_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_compute_work_group_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_compute_work_group_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_compute_work_group_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DISPATCH_INDIRECT_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_dispatch_indirect_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_dispatch_indirect_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_dispatch_indirect_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_DEBUG_GROUP_STACK_DEPTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_debug_group_stack_depth)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_debug_group_stack_depth.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_debug_group_stack_depth.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DEBUG_GROUP_STACK_DEPTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_debug_group_stack_depth)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_debug_group_stack_depth.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_debug_group_stack_depth.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_CONTEXT_FLAGS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_context_flags)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_context_flags.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_context_flags.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_CULL_FACE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_cull_face)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_cull_face.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_cull_face.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_CULL_FACE_MODE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_cull_face_mode)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_cull_face_mode.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_cull_face_mode.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_CURRENT_PROGRAM"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_current_program)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_current_program.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_current_program.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DEPTH_CLEAR_VALUE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_depth_clear_value)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_depth_clear_value.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_depth_clear_value.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DEPTH_FUNC"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_depth_func)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_depth_func.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_depth_func.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DEPTH_RANGE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_depth_range)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_depth_range.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_depth_range.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DEPTH_TEST"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_depth_test)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_depth_test.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_depth_test.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DEPTH_WRITEMASK"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_depth_writemask)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_depth_writemask.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_depth_writemask.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DITHER"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_dither)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_dither.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_dither.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DOUBLEBUFFER"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_doublebuffer)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_doublebuffer.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_doublebuffer.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DRAW_BUFFER"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_draw_buffer)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_draw_buffer.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_draw_buffer.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_DRAW_FRAMEBUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_draw_framebuffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_draw_framebuffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_draw_framebuffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_READ_FRAMEBUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_read_framebuffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_read_framebuffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_read_framebuffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_ELEMENT_ARRAY_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_element_array_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_element_array_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_element_array_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_FRAGMENT_SHADER_DERIVATIVE_HINT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_fragment_shader_derivative_hint)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_fragment_shader_derivative_hint.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_fragment_shader_derivative_hint.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_IMPLEMENTATION_COLOR_READ_FORMAT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_implementation_color_read_format)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_implementation_color_read_format.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_implementation_color_read_format.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_IMPLEMENTATION_COLOR_READ_TYPE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_implementation_color_read_type)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_implementation_color_read_type.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_implementation_color_read_type.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_LINE_SMOOTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_line_smooth)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_line_smooth.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_line_smooth.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_LINE_SMOOTH_HINT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_line_smooth_hint)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_line_smooth_hint.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_line_smooth_hint.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_LINE_WIDTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_line_width)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_line_width.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_line_width.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_LAYER_PROVOKING_VERTEX"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_layer_provoking_vertex)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_layer_provoking_vertex.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_layer_provoking_vertex.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_LOGIC_OP_MODE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_logic_op_mode)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_logic_op_mode.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_logic_op_mode.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAJOR_VERSION"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_major_version)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_major_version.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_major_version.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_3D_TEXTURE_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_3d_texture_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_3d_texture_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_3d_texture_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_ARRAY_TEXTURE_LAYERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_array_texture_layers)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_array_texture_layers.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_array_texture_layers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_CLIP_DISTANCES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_clip_distances)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_clip_distances.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_clip_distances.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COLOR_TEXTURE_SAMPLES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_color_texture_samples)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_color_texture_samples.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_color_texture_samples.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_ATOMIC_COUNTERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_atomic_counters)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_atomic_counters.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_atomic_counters.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_fragment_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_fragment_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_fragment_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_geometry_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_geometry_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_geometry_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_texture_image_units)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_texture_image_units.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_texture_image_units.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_UNIFORM_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_uniform_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_uniform_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_uniform_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_combined_vertex_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_combined_vertex_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_combined_vertex_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_CUBE_MAP_TEXTURE_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_cube_map_texture_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_cube_map_texture_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_cube_map_texture_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_DEPTH_TEXTURE_SAMPLES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_depth_texture_samples)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_depth_texture_samples.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_depth_texture_samples.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_DRAW_BUFFERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_draw_buffers)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_draw_buffers.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_draw_buffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_DUAL_SOURCE_DRAW_BUFFERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_dual_source_draw_buffers)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_dual_source_draw_buffers.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_dual_source_draw_buffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_ELEMENTS_INDICES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_elements_indices)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_elements_indices.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_elements_indices.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_ELEMENTS_VERTICES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_elements_vertices)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_elements_vertices.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_elements_vertices.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAGMENT_ATOMIC_COUNTERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_fragment_atomic_counters)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_fragment_atomic_counters.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_fragment_atomic_counters.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_fragment_shader_storage_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_fragment_shader_storage_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_fragment_shader_storage_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAGMENT_INPUT_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_fragment_input_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_fragment_input_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_fragment_input_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAGMENT_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_fragment_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_fragment_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_fragment_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAGMENT_UNIFORM_VECTORS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_fragment_uniform_vectors)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_fragment_uniform_vectors.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_fragment_uniform_vectors.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAGMENT_UNIFORM_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_fragment_uniform_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_fragment_uniform_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_fragment_uniform_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAMEBUFFER_WIDTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_framebuffer_width)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_framebuffer_width.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_framebuffer_width.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAMEBUFFER_HEIGHT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_framebuffer_height)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_framebuffer_height.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_framebuffer_height.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAMEBUFFER_LAYERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_framebuffer_layers)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_framebuffer_layers.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_framebuffer_layers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_FRAMEBUFFER_SAMPLES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_framebuffer_samples)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_framebuffer_samples.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_framebuffer_samples.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_GEOMETRY_ATOMIC_COUNTERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_geometry_atomic_counters)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_geometry_atomic_counters.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_geometry_atomic_counters.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_geometry_shader_storage_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_geometry_shader_storage_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_geometry_shader_storage_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_GEOMETRY_INPUT_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_geometry_input_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_geometry_input_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_geometry_input_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_GEOMETRY_OUTPUT_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_geometry_output_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_geometry_output_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_geometry_output_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_geometry_texture_image_units)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_geometry_texture_image_units.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_geometry_texture_image_units.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_GEOMETRY_UNIFORM_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_geometry_uniform_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_geometry_uniform_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_geometry_uniform_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_GEOMETRY_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_geometry_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_geometry_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_geometry_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_INTEGER_SAMPLES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_integer_samples)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_integer_samples.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_integer_samples.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MIN_MAP_BUFFER_ALIGNMENT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_min_map_buffer_alignment)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_min_map_buffer_alignment.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_min_map_buffer_alignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_LABEL_LENGTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_label_length)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_label_length.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_label_length.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_PROGRAM_TEXEL_OFFSET"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_program_texel_offset)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_program_texel_offset.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_program_texel_offset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MIN_PROGRAM_TEXEL_OFFSET"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_min_program_texel_offset)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_min_program_texel_offset.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_min_program_texel_offset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_RECTANGLE_TEXTURE_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_rectangle_texture_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_rectangle_texture_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_rectangle_texture_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_RENDERBUFFER_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_renderbuffer_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_renderbuffer_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_renderbuffer_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_SAMPLE_MASK_WORDS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_sample_mask_words)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_sample_mask_words.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_sample_mask_words.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_SERVER_WAIT_TIMEOUT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_server_wait_timeout)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_server_wait_timeout.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_server_wait_timeout.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_shader_storage_buffer_bindings)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_shader_storage_buffer_bindings.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_shader_storage_buffer_bindings.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_tess_control_atomic_counters)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_tess_control_atomic_counters.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_tess_control_atomic_counters.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_tess_evaluation_atomic_counters)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_tess_evaluation_atomic_counters.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_tess_evaluation_atomic_counters.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_tess_control_shader_storage_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_tess_control_shader_storage_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_tess_control_shader_storage_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_tess_evaluation_shader_storage_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_tess_evaluation_shader_storage_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_tess_evaluation_shader_storage_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TEXTURE_BUFFER_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_texture_buffer_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_texture_buffer_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_texture_buffer_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TEXTURE_IMAGE_UNITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_texture_image_units)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_texture_image_units.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_texture_image_units.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TEXTURE_LOD_BIAS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_texture_lod_bias)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_texture_lod_bias.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_texture_lod_bias.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_TEXTURE_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_texture_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_texture_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_texture_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_UNIFORM_BUFFER_BINDINGS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_uniform_buffer_bindings)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_uniform_buffer_bindings.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_uniform_buffer_bindings.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_UNIFORM_BLOCK_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_uniform_block_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_uniform_block_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_uniform_block_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_UNIFORM_LOCATIONS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_uniform_locations)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_uniform_locations.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_uniform_locations.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VARYING_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_varying_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_varying_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_varying_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VARYING_VECTORS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_varying_vectors)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_varying_vectors.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_varying_vectors.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VARYING_FLOATS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_varying_floats)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_varying_floats.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_varying_floats.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_ATOMIC_COUNTERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_atomic_counters)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_atomic_counters.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_atomic_counters.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_ATTRIBS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_attribs)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_attribs.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_attribs.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_shader_storage_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_shader_storage_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_shader_storage_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_texture_image_units)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_texture_image_units.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_texture_image_units.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_UNIFORM_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_uniform_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_uniform_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_uniform_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_UNIFORM_VECTORS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_uniform_vectors)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_uniform_vectors.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_uniform_vectors.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_OUTPUT_COMPONENTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_output_components)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_output_components.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_output_components.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_UNIFORM_BLOCKS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_uniform_blocks)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_uniform_blocks.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_uniform_blocks.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VIEWPORT_DIMS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_viewport_dims)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_viewport_dims.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_viewport_dims.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VIEWPORTS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_viewports)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_viewports.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_viewports.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MINOR_VERSION"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_minor_version)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_minor_version.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_minor_version.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_NUM_COMPRESSED_TEXTURE_FORMATS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_num_compressed_texture_formats)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_num_compressed_texture_formats.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_num_compressed_texture_formats.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_NUM_EXTENSIONS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_num_extensions)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_num_extensions.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_num_extensions.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_NUM_PROGRAM_BINARY_FORMATS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_num_program_binary_formats)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_num_program_binary_formats.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_num_program_binary_formats.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_NUM_SHADER_BINARY_FORMATS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_num_shader_binary_formats)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_num_shader_binary_formats.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_num_shader_binary_formats.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_ALIGNMENT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_alignment)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_alignment.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_alignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_IMAGE_HEIGHT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_image_height)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_image_height.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_image_height.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_LSB_FIRST"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_lsb_first)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_lsb_first.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_lsb_first.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_ROW_LENGTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_row_length)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_row_length.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_row_length.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_SKIP_IMAGES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_skip_images)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_skip_images.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_skip_images.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_SKIP_PIXELS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_skip_pixels)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_skip_pixels.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_skip_pixels.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_SKIP_ROWS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_skip_rows)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_skip_rows.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_skip_rows.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PACK_SWAP_BYTES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pack_swap_bytes)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pack_swap_bytes.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pack_swap_bytes.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PIXEL_PACK_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pixel_pack_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pixel_pack_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pixel_pack_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PIXEL_UNPACK_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_pixel_unpack_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_pixel_unpack_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_pixel_unpack_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POINT_FADE_THRESHOLD_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_point_fade_threshold_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_point_fade_threshold_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_point_fade_threshold_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PRIMITIVE_RESTART_INDEX"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_primitive_restart_index)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_primitive_restart_index.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_primitive_restart_index.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PROGRAM_BINARY_FORMATS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_program_binary_formats)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_program_binary_formats.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_program_binary_formats.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PROGRAM_PIPELINE_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_program_pipeline_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_program_pipeline_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_program_pipeline_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PROGRAM_POINT_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_program_point_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_program_point_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_program_point_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_PROVOKING_VERTEX"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_provoking_vertex)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_provoking_vertex.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_provoking_vertex.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POINT_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_point_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_point_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_point_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POINT_SIZE_GRANULARITY"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_point_size_granularity)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_point_size_granularity.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_point_size_granularity.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POINT_SIZE_RANGE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_point_size_range)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_point_size_range.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_point_size_range.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POLYGON_OFFSET_FACTOR"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_polygon_offset_factor)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_polygon_offset_factor.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_polygon_offset_factor.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POLYGON_OFFSET_UNITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_polygon_offset_units)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_polygon_offset_units.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_polygon_offset_units.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POLYGON_OFFSET_FILL"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_polygon_offset_fill)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_polygon_offset_fill.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_polygon_offset_fill.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POLYGON_OFFSET_LINE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_polygon_offset_line)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_polygon_offset_line.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_polygon_offset_line.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POLYGON_OFFSET_POINT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_polygon_offset_point)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_polygon_offset_point.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_polygon_offset_point.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POLYGON_SMOOTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_polygon_smooth)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_polygon_smooth.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_polygon_smooth.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_POLYGON_SMOOTH_HINT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_polygon_smooth_hint)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_polygon_smooth_hint.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_polygon_smooth_hint.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_READ_BUFFER"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_read_buffer)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_read_buffer.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_read_buffer.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_RENDERBUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_renderbuffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_renderbuffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_renderbuffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SAMPLE_BUFFERS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_sample_buffers)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_sample_buffers.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_sample_buffers.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SAMPLE_COVERAGE_VALUE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_sample_coverage_value)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_sample_coverage_value.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_sample_coverage_value.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SAMPLE_COVERAGE_INVERT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_sample_coverage_invert)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_sample_coverage_invert.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_sample_coverage_invert.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SAMPLER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_sampler_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_sampler_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_sampler_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SAMPLES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_samples)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_samples.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_samples.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SCISSOR_BOX"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_scissor_box)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_scissor_box.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_scissor_box.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SCISSOR_TEST"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_scissor_test)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_scissor_test.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_scissor_test.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SHADER_COMPILER"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_shader_compiler)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_shader_compiler.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_shader_compiler.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SHADER_STORAGE_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_shader_storage_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_shader_storage_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_shader_storage_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_shader_storage_buffer_offset_alignment)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_shader_storage_buffer_offset_alignment.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_shader_storage_buffer_offset_alignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SHADER_STORAGE_BUFFER_START"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_shader_storage_buffer_start)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_shader_storage_buffer_start.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_shader_storage_buffer_start.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SHADER_STORAGE_BUFFER_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_shader_storage_buffer_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_shader_storage_buffer_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_shader_storage_buffer_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SMOOTH_LINE_WIDTH_RANGE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_smooth_line_width_range)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_smooth_line_width_range.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_smooth_line_width_range.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SMOOTH_LINE_WIDTH_GRANULARITY"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_smooth_line_width_granularity)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_smooth_line_width_granularity.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_smooth_line_width_granularity.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_BACK_FAIL"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_back_fail)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_back_fail.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_back_fail.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_BACK_FUNC"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_back_func)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_back_func.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_back_func.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_BACK_PASS_DEPTH_FAIL"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_back_pass_depth_fail)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_back_pass_depth_fail.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_back_pass_depth_fail.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_BACK_PASS_DEPTH_PASS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_back_pass_depth_pass)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_back_pass_depth_pass.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_back_pass_depth_pass.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_BACK_REF"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_back_ref)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_back_ref.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_back_ref.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_BACK_VALUE_MASK"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_back_value_mask)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_back_value_mask.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_back_value_mask.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_BACK_WRITEMASK"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_back_writemask)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_back_writemask.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_back_writemask.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_CLEAR_VALUE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_clear_value)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_clear_value.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_clear_value.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_FAIL"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_fail)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_fail.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_fail.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_FUNC"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_func)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_func.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_func.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_PASS_DEPTH_FAIL"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_pass_depth_fail)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_pass_depth_fail.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_pass_depth_fail.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_PASS_DEPTH_PASS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_pass_depth_pass)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_pass_depth_pass.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_pass_depth_pass.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_REF"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_ref)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_ref.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_ref.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_TEST"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_test)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_test.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_test.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_VALUE_MASK"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_value_mask)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_value_mask.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_value_mask.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STENCIL_WRITEMASK"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stencil_writemask)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stencil_writemask.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stencil_writemask.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_STEREO"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_stereo)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_stereo.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_stereo.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_SUBPIXEL_BITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_subpixel_bits)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_subpixel_bits.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_subpixel_bits.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_1D"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_1d)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_1d.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_1d.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_1D_ARRAY"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_1d_array)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_1d_array.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_1d_array.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_2D"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_2d)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_2d.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_2d.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_2D_ARRAY"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_2d_array)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_2d_array.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_2d_array.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_2D_MULTISAMPLE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_2d_multisample)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_2d_multisample.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_2d_multisample.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_2d_multisample_array)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_2d_multisample_array.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_2d_multisample_array.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_3D"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_3d)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_3d.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_3d.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_BUFFER"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_buffer)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_buffer.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_buffer.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_CUBE_MAP"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_cube_map)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_cube_map.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_cube_map.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BINDING_RECTANGLE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_binding_rectangle)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_binding_rectangle.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_binding_rectangle.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_COMPRESSION_HINT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_compression_hint)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_compression_hint.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_compression_hint.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_texture_buffer_offset_alignment)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_texture_buffer_offset_alignment.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_texture_buffer_offset_alignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TIMESTAMP"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_timestamp)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_timestamp.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_timestamp.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TRANSFORM_FEEDBACK_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_transform_feedback_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_transform_feedback_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_transform_feedback_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TRANSFORM_FEEDBACK_BUFFER_START"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_transform_feedback_buffer_start)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_transform_feedback_buffer_start.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_transform_feedback_buffer_start.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_TRANSFORM_FEEDBACK_BUFFER_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_transform_feedback_buffer_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_transform_feedback_buffer_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_transform_feedback_buffer_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNIFORM_BUFFER_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_uniform_buffer_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_uniform_buffer_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_uniform_buffer_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_uniform_buffer_offset_alignment)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_uniform_buffer_offset_alignment.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_uniform_buffer_offset_alignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNIFORM_BUFFER_SIZE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_uniform_buffer_size)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_uniform_buffer_size.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_uniform_buffer_size.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNIFORM_BUFFER_START"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_uniform_buffer_start)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_uniform_buffer_start.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_uniform_buffer_start.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_ALIGNMENT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_alignment)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_alignment.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_alignment.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_IMAGE_HEIGHT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_image_height)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_image_height.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_image_height.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_LSB_FIRST"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_lsb_first)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_lsb_first.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_lsb_first.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_ROW_LENGTH"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_row_length)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_row_length.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_row_length.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_SKIP_IMAGES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_skip_images)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_skip_images.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_skip_images.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_SKIP_PIXELS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_skip_pixels)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_skip_pixels.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_skip_pixels.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_SKIP_ROWS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_skip_rows)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_skip_rows.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_skip_rows.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_UNPACK_SWAP_BYTES"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_unpack_swap_bytes)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_unpack_swap_bytes.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_unpack_swap_bytes.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VERTEX_ARRAY_BINDING"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_vertex_array_binding)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_vertex_array_binding.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_vertex_array_binding.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VERTEX_BINDING_DIVISOR"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_vertex_binding_divisor)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_vertex_binding_divisor.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_vertex_binding_divisor.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VERTEX_BINDING_OFFSET"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_vertex_binding_offset)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_vertex_binding_offset.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_vertex_binding_offset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VERTEX_BINDING_STRIDE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_vertex_binding_stride)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_vertex_binding_stride.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_vertex_binding_stride.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_attrib_relative_offset)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_attrib_relative_offset.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_attrib_relative_offset.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_VERTEX_ATTRIB_BINDINGS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_vertex_attrib_bindings)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_vertex_attrib_bindings.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_vertex_attrib_bindings.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VIEWPORT"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_viewport)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_viewport.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_viewport.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VIEWPORT_BOUNDS_RANGE"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_viewport_bounds_range)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_viewport_bounds_range.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_viewport_bounds_range.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VIEWPORT_INDEX_PROVOKING_VERTEX"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_viewport_index_provoking_vertex)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_viewport_index_provoking_vertex.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_viewport_index_provoking_vertex.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_VIEWPORT_SUBPIXEL_BITS"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_viewport_subpixel_bits)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_viewport_subpixel_bits.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_viewport_subpixel_bits.replace(". ", "\n").count('\n') + 1) * 48);
    ++i;

    ui->glInfoTable->insertRow(i);
    ui->glInfoTable->setItem(i, 0, new QTableWidgetItem("GL_MAX_ELEMENT_INDEX"));
    ui->glInfoTable->setItem(i, 1, new QTableWidgetItem(DISPLAY(glProfilers[0]->glInfo.gl_max_element_index)));
    ui->glInfoTable->setItem(i, 2, new QTableWidgetItem('\n' + desc.gl_max_element_index.replace(". ", "\n")));
    ui->glInfoTable->setRowHeight(i, (desc.gl_max_element_index.replace(". ", "\n").count('\n') + 1) * 48);

    ui->glInfoTable->horizontalHeader()->show();
}
