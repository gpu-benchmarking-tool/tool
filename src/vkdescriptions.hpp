/*__________________________________________________
 |                                                  |
 |    File: vkdescriptions.hpp                      |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: QString Vulkan variables         |
 |    descriptions.                                 |
 |_________________________________________________*/



#pragma once

#include <QString>


/**
 * @brief      Vulkan variables descriptions.
 * Structure containing the descriptions of the Vulkan variables.
 */
struct VkDescriptions {

    QString maxImageDimension1D = "maxImageDimension1D is the maximum dimension (width) supported for all images created with an imageType of VK_IMAGE_TYPE_1D.";

    QString maxImageDimension2D = "maxImageDimension2D is the maximum dimension (width or height) supported for all images created with an imageType of VK_IMAGE_TYPE_2D and without VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT set in flags.";

    QString maxImageDimension3D = "maxImageDimension3D is the maximum dimension (width, height, or depth) supported for all images created with an imageType of VK_IMAGE_TYPE_3D.";

    QString maxImageDimensionCube = "maxImageDimensionCube is the maximum dimension (width or height) supported for all images created with an imageType of VK_IMAGE_TYPE_2D and with VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT set in flags.";

    QString maxImageArrayLayers = "maxImageArrayLayers is the maximum number of layers (arrayLayers) for an image.";

    QString maxTexelBufferElements = "maxTexelBufferElements is the maximum number of addressable texels for a buffer view created on a buffer which was created with the VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT or VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT set in the usage member of the VkBufferCreateInfo structure.";

    QString maxUniformBufferRange = "maxUniformBufferRange is the maximum value that can be specified in the range member of any VkDescriptorBufferInfo structures passed to a call to vkUpdateDescriptorSets for descriptors of type VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER or VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC.";

    QString maxStorageBufferRange = "maxStorageBufferRange is the maximum value that can be specified in the range member of any VkDescriptorBufferInfo structures passed to a call to vkUpdateDescriptorSets for descriptors of type VK_DESCRIPTOR_TYPE_STORAGE_BUFFER or VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC.";

    QString maxPushConstantsSize = "maxPushConstantsSize is the maximum size, in bytes, of the pool of push constant memory. For each of the push constant ranges indicated by the pPushConstantRanges member of the VkPipelineLayoutCreateInfo structure, (offset + size) must be less than or equal to this limit.";

    QString maxMemoryAllocationCount = "maxMemoryAllocationCount is the maximum number of device memory allocations, as created by vkAllocateMemory, which can simultaneously exist.";

    QString maxSamplerAllocationCount = "maxSamplerAllocationCount is the maximum number of sampler objects, as created by vkCreateSampler, which can simultaneously exist on a device.";

    QString bufferImageGranularity = "bufferImageGranularity is the granularity, in bytes, at which buffer or linear image resources, and optimal image resources can be bound to adjacent offsets in the same VkDeviceMemory object without aliasing. See Buffer-Image Granularity for more details.";

    QString sparseAddressSpaceSize = "sparseAddressSpaceSize is the total amount of address space available, in bytes, for sparse memory resources. This is an upper bound on the sum of the size of all sparse resources, regardless of whether any memory is bound to them.";

    QString maxBoundDescriptorSets = "maxBoundDescriptorSets is the maximum number of descriptor sets that can be simultaneously used by a pipeline. All DescriptorSet decorations in shader modules must have a value less than maxBoundDescriptorSets.";

    QString maxPerStageDescriptorSamplers = "maxPerStageDescriptorSamplers is the maximum number of samplers that can be accessible to a single shader stage in a pipeline layout. Descriptors with a type of VK_DESCRIPTOR_TYPE_SAMPLER or VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit. A descriptor is accessible to a shader stage when the stageFlags member of the VkDescriptorSetLayoutBinding structure has the bit for that shader stage set.";

    QString maxPerStageDescriptorUniformBuffers = "maxPerStageDescriptorUniformBuffers is the maximum number of uniform buffers that can be accessible to a single shader stage in a pipeline layout. Descriptors with a type of VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER or VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit. A descriptor is accessible to a shader stage when the stageFlags member of the VkDescriptorSetLayoutBinding structure has the bit for that shader stage set.";

    QString maxPerStageDescriptorStorageBuffers = "maxPerStageDescriptorStorageBuffers is the maximum number of storage buffers that can be accessible to a single shader stage in a pipeline layout. Descriptors with a type of VK_DESCRIPTOR_TYPE_STORAGE_BUFFER or VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit. A descriptor is accessible to a pipeline shader stage when the stageFlags member of the VkDescriptorSetLayoutBinding structure has the bit for that shader stage set.";

    QString maxPerStageDescriptorSampledImages = "maxPerStageDescriptorSampledImages is the maximum number of sampled images that can be accessible to a single shader stage in a pipeline layout. Descriptors with a type of VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, or VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit. A descriptor is accessible to a pipeline shader stage when the stageFlags member of the VkDescriptorSetLayoutBinding structure has the bit for that shader stage set.";

    QString maxPerStageDescriptorStorageImages = "maxPerStageDescriptorStorageImages is the maximum number of storage images that can be accessible to a single shader stage in a pipeline layout. Descriptors with a type of VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, or VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit. A descriptor is accessible to a pipeline shader stage when the stageFlags member of the VkDescriptorSetLayoutBinding structure has the bit for that shader stage set.";

    QString maxPerStageDescriptorInputAttachments = "maxPerStageDescriptorInputAttachments is the maximum number of input attachments that can be accessible to a single shader stage in a pipeline layout. Descriptors with a type of VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit. A descriptor is accessible to a pipeline shader stage when the stageFlags member of the VkDescriptorSetLayoutBinding structure has the bit for that shader stage set. These are only supported for the fragment stage.";

    QString maxPerStageResources = "maxPerStageResources is the maximum number of resources that can be accessible to a single shader stage in a pipeline layout. Descriptors with a type of VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, or VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit. For the fragment shader stage the framebuffer color attachments also count against this limit.";

    QString maxDescriptorSetSamplers = "maxDescriptorSetSamplers is the maximum number of samplers that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_SAMPLER or VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxDescriptorSetUniformBuffers = "maxDescriptorSetUniformBuffers is the maximum number of uniform buffers that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER or VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxDescriptorSetUniformBuffersDynamic = "maxDescriptorSetUniformBuffersDynamic is the maximum number of dynamic uniform buffers that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxDescriptorSetStorageBuffers = "maxDescriptorSetStorageBuffers is the maximum number of storage buffers that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_STORAGE_BUFFER or VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxDescriptorSetStorageBuffersDynamic = "maxDescriptorSetStorageBuffersDynamic is the maximum number of dynamic storage buffers that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxDescriptorSetSampledImages = "maxDescriptorSetSampledImages is the maximum number of sampled images that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, or VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxDescriptorSetStorageImages = "maxDescriptorSetStorageImages is the maximum number of storage images that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, or VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxDescriptorSetInputAttachments = "maxDescriptorSetInputAttachments is the maximum number of input attachments that can be included in descriptor bindings in a pipeline layout across all pipeline shader stages and descriptor set numbers. Descriptors with a type of VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT count against this limit. Only descriptors in descriptor set layouts created without the VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT bit set count against this limit.";

    QString maxVertexInputAttributes = "maxVertexInputAttributes is the maximum number of vertex input attributes that can be specified for a graphics pipeline. These are described in the array of VkVertexInputAttributeDescription structures that are provided at graphics pipeline creation time via the pVertexAttributeDescriptions member of the VkPipelineVertexInputStateCreateInfo structure.";

    QString maxVertexInputBindings = "maxVertexInputBindings is the maximum number of vertex buffers that can be specified for providing vertex attributes to a graphics pipeline. These are described in the array of VkVertexInputBindingDescription structures that are provided at graphics pipeline creation time via the pVertexBindingDescriptions member of the VkPipelineVertexInputStateCreateInfo structure. The binding member of VkVertexInputBindingDescription must be less than this limit.";

    QString maxVertexInputAttributeOffset = "maxVertexInputAttributeOffset is the maximum vertex input attribute offset that can be added to the vertex input binding stride. The offset member of the VkVertexInputAttributeDescription structure must be less than or equal to this limit.";

    QString maxVertexInputBindingStride = "maxVertexInputBindingStride is the maximum vertex input binding stride that can be specified in a vertex input binding. The stride member of the VkVertexInputBindingDescription structure must be less than or equal to this limit.";

    QString maxVertexOutputComponents = "maxVertexOutputComponents is the maximum number of components of output variables which can be output by a vertex shader.";

    QString maxTessellationGenerationLevel = "maxTessellationGenerationLevel is the maximum tessellation generation level supported by the fixed-function tessellation primitive generator.";

    QString maxTessellationPatchSize = "maxTessellationPatchSize is the maximum patch size, in vertices, of patches that can be processed by the tessellation control shader and tessellation primitive generator. The patchControlPoints member of the VkPipelineTessellationStateCreateInfo structure specified at pipeline creation time and the value provided in the OutputVertices execution mode of shader modules must be less than or equal to this limit.";

    QString maxTessellationControlPerVertexInputComponents = "maxTessellationControlPerVertexInputComponents is the maximum number of components of input variables which can be provided as per-vertex inputs to the tessellation control shader stage.";

    QString maxTessellationControlPerVertexOutputComponents = "maxTessellationControlPerVertexOutputComponents is the maximum number of components of per-vertex output variables which can be output from the tessellation control shader stage.";

    QString maxTessellationControlPerPatchOutputComponents = "maxTessellationControlPerPatchOutputComponents is the maximum number of components of per-patch output variables which can be output from the tessellation control shader stage.";

    QString maxTessellationControlTotalOutputComponents = "maxTessellationControlTotalOutputComponents is the maximum total number of components of per-vertex and per-patch output variables which can be output from the tessellation control shader stage.";

    QString maxTessellationEvaluationInputComponents = "maxTessellationEvaluationInputComponents is the maximum number of components of input variables which can be provided as per-vertex inputs to the tessellation evaluation shader stage.";

    QString maxTessellationEvaluationOutputComponents = "maxTessellationEvaluationOutputComponents is the maximum number of components of per-vertex output variables which can be output from the tessellation evaluation shader stage.";

    QString maxGeometryShaderInvocations = "maxGeometryShaderInvocations is the maximum invocation count supported for instanced geometry shaders. The value provided in the Invocations execution mode of shader modules must be less than or equal to this limit.";

    QString maxGeometryInputComponents = "maxGeometryInputComponents is the maximum number of components of input variables which can be provided as inputs to the geometry shader stage.";

    QString maxGeometryOutputComponents = "maxGeometryOutputComponents is the maximum number of components of output variables which can be output from the geometry shader stage.";

    QString maxGeometryOutputVertices = "maxGeometryOutputVertices is the maximum number of vertices which can be emitted by any geometry shader.";

    QString maxGeometryTotalOutputComponents = "maxGeometryTotalOutputComponents is the maximum total number of components of output, across all emitted vertices, which can be output from the geometry shader stage.";

    QString maxFragmentInputComponents = "maxFragmentInputComponents is the maximum number of components of input variables which can be provided as inputs to the fragment shader stage.";

    QString maxFragmentOutputAttachments = "maxFragmentOutputAttachments is the maximum number of output attachments which can be written to by the fragment shader stage.";

    QString maxFragmentDualSrcAttachments = "maxFragmentDualSrcAttachments is the maximum number of output attachments which can be written to by the fragment shader stage when blending is enabled and one of the dual source blend modes is in use.";

    QString maxFragmentCombinedOutputResources = "maxFragmentCombinedOutputResources is the total number of storage buffers, storage images, and output buffers which can be used in the fragment shader stage.";

    QString maxComputeSharedMemorySize = "maxComputeSharedMemorySize is the maximum total storage size, in bytes, available for variables declared with the Workgroup storage class in shader modules (or with the shared storage qualifier in GLSL) in the compute shader stage. The amount of storage consumed by the variables declared with the Workgroup storage class is implementation-dependent. However, the amount of storage consumed may not exceed the largest block size that would be obtained if all active variables declared with Workgroup storage class were assigned offsets in an arbitrary order by successively taking the smallest valid offset according to the Standard Storage Buffer Layout rules. (This is equivalent to using the GLSL std430 layout rules.)";

    QString maxComputeWorkGroupCountX = "maxComputeWorkGroupCount is the maximum number of local workgroups that can be dispatched by a single dispatch command. These three values represent the maximum number of local workgroups for the X, Y, and Z dimensions, respectively. The workgroup count parameters to the dispatch commands must be less than or equal to the corresponding limit.";

    QString maxComputeWorkGroupCountY = "maxComputeWorkGroupCount is the maximum number of local workgroups that can be dispatched by a single dispatch command. These three values represent the maximum number of local workgroups for the X, Y, and Z dimensions, respectively. The workgroup count parameters to the dispatch commands must be less than or equal to the corresponding limit.";

    QString maxComputeWorkGroupCountZ = "maxComputeWorkGroupCount is the maximum number of local workgroups that can be dispatched by a single dispatch command. These three values represent the maximum number of local workgroups for the X, Y, and Z dimensions, respectively. The workgroup count parameters to the dispatch commands must be less than or equal to the corresponding limit.";

    QString maxComputeWorkGroupInvocations = "maxComputeWorkGroupInvocations is the maximum total number of compute shader invocations in a single local workgroup. The product of the X, Y, and Z sizes, as specified by the LocalSize execution mode in shader modules or by the object decorated by the WorkgroupSize decoration, must be less than or equal to this limit.";

    QString maxComputeWorkGroupSizeX = "maxComputeWorkGroupSize is the maximum size of a local compute workgroup, per dimension. These three values represent the maximum local workgroup size in the X, Y, and Z dimensions, respectively. The x, y, and z sizes, as specified by the LocalSize execution mode or by the object decorated by the WorkgroupSize decoration in shader modules, must be less than or equal to the corresponding limit.";

    QString maxComputeWorkGroupSizeY = "maxComputeWorkGroupSize is the maximum size of a local compute workgroup, per dimension. These three values represent the maximum local workgroup size in the X, Y, and Z dimensions, respectively. The x, y, and z sizes, as specified by the LocalSize execution mode or by the object decorated by the WorkgroupSize decoration in shader modules, must be less than or equal to the corresponding limit.";

    QString maxComputeWorkGroupSizeZ = "maxComputeWorkGroupSize is the maximum size of a local compute workgroup, per dimension. These three values represent the maximum local workgroup size in the X, Y, and Z dimensions, respectively. The x, y, and z sizes, as specified by the LocalSize execution mode or by the object decorated by the WorkgroupSize decoration in shader modules, must be less than or equal to the corresponding limit.";

    QString subPixelPrecisionBits = "subPixelPrecisionBits is the number of bits of subpixel precision in framebuffer coordinates xf and yf.";

    QString subTexelPrecisionBits = "subTexelPrecisionBits is the number of bits of precision in the division along an axis of an image used for minification and magnification filters.";

    QString mipmapPrecisionBits = "mipmapPrecisionBits is the number of bits of division that the LOD calculation for mipmap fetching get snapped to when determining the contribution from each mip level to the mip filtered results.";

    QString maxDrawIndexedIndexValue = "maxDrawIndexedIndexValue is the maximum index value that can be used for indexed draw calls when using 32-bit indices. This excludes the primitive restart index value of 0xFFFFFFFF. See fullDrawIndexUint32.";

    QString maxDrawIndirectCount = "maxDrawIndirectCount is the maximum draw count that is supported for indirect draw calls. See multiDrawIndirect.";

    QString maxSamplerLodBias = "maxSamplerLodBias is the maximum absolute sampler LOD bias. The sum of the mipLodBias member of the VkSamplerCreateInfo structure and the Bias operand of image sampling operations in shader modules (or 0 if no Bias operand is provided to an image sampling operation) are clamped to the range [-maxSamplerLodBias,+maxSamplerLodBias].";

    QString maxSamplerAnisotropy = "maxSamplerAnisotropy is the maximum degree of sampler anisotropy. The maximum degree of anisotropic filtering used for an image sampling operation is the minimum of the maxAnisotropy member of the VkSamplerCreateInfo structure and this limit.";

    QString maxViewports = "maxViewports is the maximum number of active viewports. The viewportCount member of the VkPipelineViewportStateCreateInfo structure that is provided at pipeline creation must be less than or equal to this limit.";

    QString maxViewportDimensionsX = "maxViewportDimensions are the maximum viewport dimensions in the X (width) and Y (height) dimensions, respectively. The maximum viewport dimensions must be greater than or equal to the largest image which can be created and used as a framebuffer attachment. See Controlling the Viewport.";

    QString maxViewportDimensionsY = "maxViewportDimensions are the maximum viewport dimensions in the X (width) and Y (height) dimensions, respectively. The maximum viewport dimensions must be greater than or equal to the largest image which can be created and used as a framebuffer attachment. See Controlling the Viewport.";

    QString viewportBoundsRangeX = "viewportBoundsRange is the [minimum, maximum] range that the corners of a viewport must be contained in. This range must be at least [-2 × size, 2 × size - 1], where size = max(maxViewportDimensions[0], maxViewportDimensions[1]). See Controlling the Viewport.";

    QString viewportBoundsRangeY = "viewportBoundsRange is the [minimum, maximum] range that the corners of a viewport must be contained in. This range must be at least [-2 × size, 2 × size - 1], where size = max(maxViewportDimensions[0], maxViewportDimensions[1]). See Controlling the Viewport.";

    QString viewportSubPixelBits = "viewportSubPixelBits is the number of bits of subpixel precision for viewport bounds. The subpixel precision that floating - point viewport bounds are interpreted at is given by this limit.";

    QString minMemoryMapAlignment = "minMemoryMapAlignment is the minimum required alignment, in bytes, of host visible memory allocations within the host address space. When mapping a memory allocation with vkMapMemory, subtracting offset bytes from the returned pointer will always produce an integer multiple of this limit.";

    QString minTexelBufferOffsetAlignment = "minTexelBufferOffsetAlignment is the minimum required alignment, in bytes, for the offset member of the VkBufferViewCreateInfo structure for texel buffers. If texelBufferAlignment is enabled, this limit is equivalent to the maximum of the uniformTexelBufferOffsetAlignmentBytes and storageTexelBufferOffsetAlignmentBytes members of VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT, but smaller alignment is optionally: allowed by storageTexelBufferOffsetSingleTexelAlignment and uniformTexelBufferOffsetSingleTexelAlignment. If texelBufferAlignment is not enabled, VkBufferViewCreateInfo::offset must be a multiple of this value.";

    QString minUniformBufferOffsetAlignment = "minUniformBufferOffsetAlignment is the minimum required alignment, in bytes, for the offset member of the VkDescriptorBufferInfo structure for uniform buffers. When a desc =riptor of type VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER or VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC is updated, the offset must be an integer multiple of this limit. Similarly, dynamic offsets for uniform buffers must be multiples of this limit.";

    QString minStorageBufferOffsetAlignment = "minStorageBufferOffsetAlignment is the minimum required alignment, in bytes, for the offset member of the VkDescriptorBufferInfo structure for storage buffers. When a desc =riptor of type VK_DESCRIPTOR_TYPE_STORAGE_BUFFER or VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC is updated, the offset must be an integer multiple of this limit. Similarly, dynamic offsets for storage buffers must be multiples of this limit.";

    QString minTexelOffset = "minTexelOffset is the minimum offset value for the ConstOffset image operand of any of the OpImageSample* or OpImageFetch* image instructions.";

    QString maxTexelOffset = "maxTexelOffset is the maximum offset value for the ConstOffset image operand of any of the OpImageSample* or OpImageFetch* image instructions.";

    QString minTexelGatherOffset = "minTexelGatherOffset is the minimum offset value for the Offset or ConstOffsets image operands of any of the OpImage*Gather image instructions.";

    QString maxTexelGatherOffset = "maxTexelGatherOffset is the maximum offset value for the Offset or ConstOffsets image operands of any of the OpImage*Gather image instructions.";

    QString minInterpolationOffset = "minInterpolationOffset is the minimum negative offset value for the offset operand of the InterpolateAtOffset extended instruction.";

    QString maxInterpolationOffset = "maxInterpolationOffset is the maximum positive offset value for the offset operand of the InterpolateAtOffset extended instruction.";

    QString subPixelInterpolationOffsetBits = "subPixelInterpolationOffsetBits is the number of subpixel fractional bits that the x and y offsets to the InterpolateAtOffset extended instruction may be rounded to as fixed-point values.";

    QString maxFramebufferWidth = "maxFramebufferWidth is the maximum width for a framebuffer. The width member of the VkFramebufferCreateInfo structure must be less than or equal to this limit.";

    QString maxFramebufferHeight = "maxFramebufferHeight is the maximum height for a framebuffer. The height member of the VkFramebufferCreateInfo structure must be less than or equal to this limit.";

    QString maxFramebufferLayers = "maxFramebufferLayers is the maximum layer count for a layered framebuffer. The layers member of the VkFramebufferCreateInfo structure must be less than or equal to this limit.";

    QString framebufferColorSampleCounts = "framebufferColorSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the color sample counts that are supported for all framebuffer color attachments with floating - or fixed - point formats. There is no limit that specifies the color sample counts that are supported for all color attachments with integer formats.";

    QString framebufferDepthSampleCounts = "framebufferDepthSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the supported depth sample counts for all framebuffer depth/stencil attachments, when the format includes a depth component.";

    QString framebufferStencilSampleCounts = "framebufferStencilSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the supported stencil sample counts for all framebuffer depth / stencil attachments, when the format includes a stencil component.";

    QString framebufferNoAttachmentsSampleCounts = "framebufferNoAttachmentsSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the supported sample counts for a framebuffer with no attachments.";

    QString maxColorAttachments = "maxColorAttachments is the maximum number of color attachments that can be used by a subpass in a render pass. The colorAttachmentCount member of the VkSubpassDescription structure must be less than or equal to this limit.";

    QString sampledImageColorSampleCounts = "sampledImageColorSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the sample counts supported for all 2D images created with VK_IMAGE_TILING_OPTIMAL, usage containing VK_IMAGE_USAGE_SAMPLED_BIT, and a non - integer color format.";

    QString sampledImageIntegerSampleCounts = "sampledImageIntegerSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the sample counts supported for all 2D images created with VK_IMAGE_TILING_OPTIMAL, usage containing VK_IMAGE_USAGE_SAMPLED_BIT, and an integer color format.";

    QString sampledImageDepthSampleCounts = "sampledImageDepthSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the sample counts supported for all 2D images created with VK_IMAGE_TILING_OPTIMAL, usage containing VK_IMAGE_USAGE_SAMPLED_BIT, and a depth format.";

    QString sampledImageStencilSampleCounts = "sampledImageStencilSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the sample supported for all 2D images created with VK_IMAGE_TILING_OPTIMAL, usage containing VK_IMAGE_USAGE_SAMPLED_BIT, and a stencil format.";

    QString storageImageSampleCounts = "storageImageSampleCounts is a bitmask1 of VkSampleCountFlagBits indicating the sample counts supported for all 2D images created with VK_IMAGE_TILING_OPTIMAL, and usage containing VK_IMAGE_USAGE_STORAGE_BIT.";

    QString maxSampleMaskWords = "maxSampleMaskWords is the maximum number of array elements of a variable decorated with the SampleMask built - in decoration.";

    QString timestampComputeAndGraphics = "timestampComputeAndGraphics specifies support for timestamps on all graphics and compute queues. If this limit is set to VK_TRUE, all queues that advertise the VK_QUEUE_GRAPHICS_BIT or VK_QUEUE_COMPUTE_BIT in the VkQueueFamilyProperties::queueFlags support VkQueueFamilyProperties::timestampValidBits of at least 36. See Timestamp Queries.";

    QString timestampPeriod = "timestampPeriod is the number of nanoseconds required for a timestamp query to be incremented by 1. See Timestamp Queries.";

    QString maxClipDistances = "maxClipDistances is the maximum number of clip distances that can be used in a single shader stage. The size of any array declared with the ClipDistance built-in decoration in a shader module must be less than or equal to this limit.";

    QString maxCullDistances = "maxCullDistances is the maximum number of cull distances that can be used in a single shader stage. The size of any array declared with the CullDistance built-in decoration in a shader module must be less than or equal to this limit.";

    QString maxCombinedClipAndCullDistances = "maxCombinedClipAndCullDistances is the maximum combined number of clip and cull distances that can be used in a single shader stage. The sum of the sizes of any pair of arrays declared with the ClipDistance and CullDistance built-in decoration used by a single shader stage in a shader module must be less than or equal to this limit.";

    QString discreteQueuePriorities = "discreteQueuePriorities is the number of discrete priorities that can be assigned to a queue based on the value of each member of VkDeviceQueueCreateInfo::pQueuePriorities. This must be at least 2, and levels must be spread evenly over the range, with at least one level at 1.0, and another at 0.0.";

    QString pointSizeRangeX = "pointSizeRange is the range [minimum,maximum] of supported sizes for points. Values written to variables decorated with the PointSize built-in decoration are clamped to this range.";

    QString pointSizeRangeY = "pointSizeRange is the range [minimum,maximum] of supported sizes for points. Values written to variables decorated with the PointSize built-in decoration are clamped to this range.";

    QString lineWidthRangeX = "lineWidthRange is the range [minimum,maximum] of supported widths for lines. Values specified by the lineWidth member of the VkPipelineRasterizationStateCreateInfo or the lineWidth parameter to vkCmdSetLineWidth are clamped to this range.";

    QString lineWidthRangeY = "lineWidthRange is the range [minimum,maximum] of supported widths for lines. Values specified by the lineWidth member of the VkPipelineRasterizationStateCreateInfo or the lineWidth parameter to vkCmdSetLineWidth are clamped to this range.";

    QString pointSizeGranularity = "pointSizeGranularity is the granularity of supported point sizes. Not all point sizes in the range defined by pointSizeRange are supported. This limit specifies the granularity (or increment) between successive supported point sizes.";

    QString lineWidthGranularity = "lineWidthGranularity is the granularity of supported line widths. Not all line widths in the range defined by lineWidthRange are supported. This limit specifies the granularity (or increment) between successive supported line widths.";

    QString strictLines = "strictLines specifies whether lines are rasterized according to the preferred method of rasterization. If set to VK_FALSE, lines may be rasterized under a relaxed set of rules. If set to VK_TRUE, lines are rasterized as per the strict definition. See Basic Line Segment Rasterization.";

    QString standardSampleLocations = "standardSampleLocations specifies whether rasterization uses the standard sample locations as documented in Multisampling. If set to VK_TRUE, the implementation uses the documented sample locations. If set to VK_FALSE, the implementation may use different sample locations.";

    QString optimalBufferCopyOffsetAlignment = "optimalBufferCopyOffsetAlignment is the optimal buffer offset alignment in bytes for vkCmdCopyBufferToImage and vkCmdCopyImageToBuffer. The per texel alignment requirements are enforced, but applications should use the optimal alignment for optimal performance and power use.";

    QString optimalBufferCopyRowPitchAlignment = "optimalBufferCopyRowPitchAlignment is the optimal buffer row pitch alignment in bytes for vkCmdCopyBufferToImage and vkCmdCopyImageToBuffer. Row pitch is the number of bytes between texels with the same X coordinate in adjacent rows (Y coordinates differ by one). The per texel alignment requirements are enforced, but applications should use the optimal alignment for optimal performance and power use.";

    QString nonCoherentAtomSize = "nonCoherentAtomSize is the size and alignment in bytes that bounds concurrent access to host - mapped device memory.";
};
