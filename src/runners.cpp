/*__________________________________________________
 |                                                  |
 |    File: runners.cpp                             |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: Benchmark scripts.               |
 |_________________________________________________*/



#include "mainwindow.h"
#include "ui_toolwindow.h"


// Runs benchmarking.
void MainWindow::run() {

    // We have to run both OpenGL and Vulkan
    twoRuns = true;

    // Run OpenGL Benchmark
    runGL();

    // Run Vulkan Benchamrk
    runVk();
}


// Runs OpenGL benchmark.
void MainWindow::runGL() {

    /*----- RUN 1 -----*/
    // Create the GL Window
    glWindow = new GLWindow(this);
    glWindow->setAttribute(Qt::WA_DeleteOnClose);
    glWindow->setProfiler(glProfilers[0]);
    glWindow->setTargetFramerate(spinBox->value());
    glWindow->resize(500, 500);
    glWindow->show();

    // Wait for OpenGL to finish
    QEventLoop loop0;
    connect(glWindow, SIGNAL(destroyed()), &loop0, SLOT(quit()));
    loop0.exec();

    // Update the progress bar
    if (twoRuns) ui->progressBar->setValue(20);
    else ui->progressBar->setValue(33);


    /*----- RUN 2 -----*/
    // Create the GL Window
    glWindow = new GLWindow(this);
    glWindow->setAttribute(Qt::WA_DeleteOnClose);
    glWindow->setProfiler(glProfilers[1]);
    glWindow->setTargetFramerate(spinBox->value());
    glWindow->resize(500, 500);
    glWindow->show();

    // Wait for OpenGL to finish
    QEventLoop loop1;
    connect(glWindow, SIGNAL(destroyed()), &loop1, SLOT(quit()));
    loop1.exec();

    // Update the progress bar
    if (twoRuns) ui->progressBar->setValue(40);
    else ui->progressBar->setValue(66);


    /*----- RUN 3 -----*/
    // Create the GL Window
    glWindow = new GLWindow(this);
    glWindow->setAttribute(Qt::WA_DeleteOnClose);
    glWindow->setProfiler(glProfilers[2]);
    glWindow->setTargetFramerate(spinBox->value());
    glWindow->resize(500, 500);
    glWindow->show();

    // Wait for OpenGL to finish
    QEventLoop loop2;
    connect(glWindow, SIGNAL(destroyed()), &loop2, SLOT(quit()));
    loop2.exec();


    // Update the GL info table
    updateGLInfoTable();

    // Update the GL charts
    updateGLCharts();

    // Update GL card
    updateGLCard();

    // Enable GL tabs
    ui->tabWidget->setTabEnabled(1, true);
    ui->tabWidget->setTabEnabled(2, true);
    ui->tabWidget->setTabEnabled(3, true);
    if (!twoRuns) ui->infoStacked->setCurrentIndex(1);

    // Update the progress bar
    if (twoRuns) ui->progressBar->setValue(60);
    else ui->progressBar->setValue(100);

    // We have results for OpenGL
    hasGLResults = true;
}


// Runs Vulkan benchmark.
void MainWindow::runVk() {

    /*----- RUN 1 -----*/
    // Create the Vulkan window
    vkWindow = new VulkanWindow(vkProfilers[0], spinBox->value());
    vkWindow->resize(500, 500);
    vkWindow->show();

    // Wait for Vulkan to finish
    QEventLoop loop0;
    QThread thread0;
    QObject context0;
    context0.moveToThread(&thread0);

    connect(&thread0, &QThread::started, &context0, [&]() {
        while (vkWindow->isVisible());
        loop0.quit();
    });
    thread0.start();
    loop0.exec();
    thread0.terminate();
    thread0.wait();

    delete vkWindow;

    // Update the progress bar
    if (twoRuns) ui->progressBar->setValue(80);
    else ui->progressBar->setValue(50);


    /*----- RUN 2 -----*/
    // Create the Vulkan window
    vkWindow = new VulkanWindow(vkProfilers[1], spinBox->value());
    vkWindow->resize(500, 500);
    vkWindow->show();

    // Wait for Vulkan to finish
    QEventLoop loop1;
    QThread thread1;
    QObject context1;
    context1.moveToThread(&thread1);

    connect(&thread1, &QThread::started, &context1, [&]() {
        while (vkWindow->isVisible());
        loop1.quit();
    });
    thread1.start();
    loop1.exec();
    thread1.terminate();
    thread1.wait();

    delete vkWindow;

    // Update the Vulkan info table
    updateVkInfoTable();

    // Update the Vulkan charts
    updateVkCharts();

    // Update Vulkan card
    updateVkCard();

    // Enable Vulkan tabs
    ui->tabWidget->setTabEnabled(4, true);
    ui->tabWidget->setTabEnabled(5, true);
    ui->tabWidget->setTabEnabled(6, true);
    ui->infoStacked->setCurrentIndex(1);

    // Update the progress bar
    ui->progressBar->setValue(100);

    // We have results ofr vulkan.
    hasVulkanResults = true;
}
