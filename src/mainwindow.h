/*__________________________________________________
 |                                                  |
 |  File: mainwindow.cpp                            |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: MainWindow class.                  |
 |_________________________________________________*/



#pragma once


#include <QMainWindow>
#include <QTableWidgetItem>
#include <QThread>
#include <QSpinBox>
#include <QtCharts>
using namespace QtCharts;

#include "libs/glwindow.h"
#include "libs/vulkanwindow.h"


/**
 * UI namespace.
 */
namespace Ui {
class MainWindow;
}



/**
 * @brief      Struct used to represent a chart.
 * Contains QChartView, QChart, QValueAxis and QLineSeries.
 */
struct Chart {
    QChartView *chartView;
    QChart *chart;
    QValueAxis *axisX, *axisY;
    QLineSeries *series;
};


/**
 * @brief      Class for main window.
 * Main Window based on QMainWindow Widget.
 * Contains the profilers and the charts.
 */
class MainWindow : public QMainWindow {

    Q_OBJECT

  private:

    Ui::MainWindow *ui;
    GLWindow *glWindow;
    VulkanWindow *vkWindow;
    gpu::Profiler *glProfilers[3], *vkProfilers[2];
    Chart glCharts[3], vkCharts[2];
    bool twoRuns, hasGLResults, hasVulkanResults;
    QSpinBox *spinBox;


    /**
     * @brief      Updates the OpenGL info table.
     */
    void updateGLInfoTable();


    /**
     * @brief      Updates the OpenGL charts.
     */
    void updateGLCharts();


    /**
     * @brief      Updates the OpenGL result card.
     */
    void updateGLCard();


    /**
     * @brief      Updates the Vulkan info table.
     */
    void updateVkInfoTable();


    /**
     * @brief      Updates the Vulkan charts.
     */
    void updateVkCharts();


    /**
     * @brief      Updates the Vulkan result card.
     */
    void updateVkCard();


  public:

    /**
     * @brief      Constructs the object.
     * Builds the Main Window.
     *
     * @param      parent  The parent window (null in this case)
     */
    MainWindow(QWidget *parent = nullptr);


    /**
     * @brief      Destroys the object.
     */
    ~MainWindow();


  private:

    /**
     * @brief      Resets the program.
     * Empty fields, charts and resets data.
     */
    void reset();


    /**
     * @brief      Connects the buttons.
     * Connects the UI buttons to the slot functions.
     */
    void connectButtons();


    /**
     * @brief      Setups the charts.
     * Prepare the charts so that the data can be added when the benchmarkings are done.
     */
    void setupCharts();


  private slots:

    /**
     * @brief      Runs benchmarking script.
     * Runs 3 OpenGL runs and 2 Vulkan runs.
     */
    void run();


    /**
     * @brief      Runs OpenGL Benchmark.
     * Runs 3 OpenGL benchmarks.
     */
    void runGL();


    /**
     * @brief      Runs Vulkan Benchmark.
     * Runs 2 Vulkan benchmarks.
     */
    void runVk();


    /**
     * @brief      Exports the results.
     * Exports the results as PNGs and CSVs.
     */
    void exportResults();
};
