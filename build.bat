rmdir /s /Q build
mkdir build
cd build
qmake ../Tool.pro -platform win32-g++
mingw32-make
cd ..

rmdir /s /Q bin
mkdir bin
cd bin
mkdir assets
cd ..
copy build\release\Tool.exe bin
copy assets\OBJ\*.obj bin\assets
cd bin
windeployqt Tool.exe