
cp qt-installer-noninteractive.qs /usr/not-backed-up/
cd /usr/not-backed-up


echo "Clearing folder"
rm -rf Qt5.13/
rm -rf 1.1.108.0
rm qt-opensource-linux-x64-5.13.0.run
rm vulkansdk-linux-x86_64-1.1.108.0.tar.gz


echo "Downloading and installing Qt 5.13"
wget http://mirrors.ukfast.co.uk/sites/qt.io/archive/qt/5.13/5.13.0/qt-opensource-linux-x64-5.13.0.run
chmod +x qt-opensource-linux-x64-5.13.0.run
./qt-opensource-linux-x64-5.13.0.run --script qt-installer-noninteractive.qs


echo "Adding Qt to path"
export PATH="/usr/not-backed-up/Qt5.13/5.13.0/gcc_64/bin:$PATH"
export LD_LIBRARY_PATH='/usr/not-backed-up/Qt5.13/5.13.0/gcc_64/lib':$LD_LIBRARY_PATH;


echo "Downloading and installing Vulkan"
wget https://sdk.lunarg.com/sdk/download/1.1.108.0/linux/vulkansdk-linux-x86_64-1.1.108.0.tar.gz
tar -xvzf vulkansdk-linux-x86_64-1.1.108.0.tar.gz


echo "Adding Vulkan to path"
export VULKAN_SDK="/usr/not-backed-up/1.1.108.0/x86_64"
export PATH="/usr/not-backed-up/1.1.108.0/x86_64/bin:$PATH"
export LD_LIBRARY_PATH='/usr/not-backed-up/1.1.108.0/x86_64/lib':$LD_LIBRARY_PATH;
export VK_LAYER_PATH="/usr/not-backed-up/1.1.108.0/x86_64/etc/vulkan/explicit_layer.d"


echo "Setting up dependencies"
cd -
./setup.sh


echo "Building"
rm -rf build bin
mkdir build && mkdir bin && mkdir bin/assets
cd build && qmake ../Tool.pro INCLUDEPATH+=/usr/not-backed-up/1.1.108.0/x86_64/include && make
cd .. && cp build/Tool bin && cp assets/OBJ/*.obj bin/assets


echo "To run the software do:"
echo "cd bin && ./Tool"
echo "If you have the error 'Failed to create Vulkan instance: 0' do the following before running Tool:"
echo "export LD_LIBRARY_PATH='/usr/not-backed-up/1.1.108.0/x86_64/lib':$LD_LIBRARY_PATH;"
